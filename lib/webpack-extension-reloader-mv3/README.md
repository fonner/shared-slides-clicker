# Fix for webpack-extension-reloader

This is a fork of [webpack-extension-reloader](https://github.com/rubenspgcavalcante/webpack-extension-reloader/) that fixes [Issue #125 Manifest V3 Service worker registration failed](https://github.com/rubenspgcavalcante/webpack-extension-reloader/issues/125)

The fix was created by [khlevon](https://github.com/khlevon/webpack-extension-reloader/tree/fix_issue_125).
