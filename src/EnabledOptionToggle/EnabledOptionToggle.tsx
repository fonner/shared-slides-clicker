import * as Sentry from '@sentry/browser';
import { ToggleEnabledResponseMessage } from '../@types/messages';
import OptionToggle from '../OptionToggle';
import { backgroundToggleEnabled } from '../utils/messageUtils';

interface EnabledOptionToggleProps {
  isEnabled: boolean;
  setIsEnabled: (isEnabled: boolean) => void;
  setShowRefreshMessage: (showMessage: boolean) => void;
  isDisabled?: boolean;
}
export const EnabledOptionToggle = ({
  isEnabled = true,
  setIsEnabled,
  setShowRefreshMessage,
  isDisabled = false,
}: EnabledOptionToggleProps): JSX.Element => {
  const handleEnabledToggleResponse = (message: ToggleEnabledResponseMessage) => {
    if (message && message.success) {
      const { isEnabled: messageIsEnabled } = message.response;
      setIsEnabled(messageIsEnabled);
      setShowRefreshMessage(true);
      console.log(`Extension is ${messageIsEnabled ? 'enabled' : 'disabled'}`);
    } else {
      Sentry.captureMessage('Unable to get enabled status');
      console.error('Unable to get enabled status');
    }
  };

  const handleError = (error: Error) => {
    Sentry.captureException(error, {
      extra: { errorMessage: 'EnabledOptionToggle Error from Background' },
    });
    console.error(`Error from background script: ${error}`);
  };
  const toggleEnabled = async () => {
    return backgroundToggleEnabled(handleEnabledToggleResponse, handleError);
  };
  return (
    <OptionToggle
      label="Enabled"
      enabledMessage="Shared Slides Clicker is currently enabled and ready to use. See instructions below for more information."
      disabledMessage="While disabled, you cannot share your presentations or control presentations shared by others."
      isEnabled={isEnabled}
      doBackgroundToggle={toggleEnabled}
      isDisabled={isDisabled}
    />
  );
};
