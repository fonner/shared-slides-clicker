import * as Sentry from '@sentry/browser';
import { User } from 'firebase/auth';
import { serverTimestamp } from 'firebase/database';
import { backgroundSendBrowserNotification } from '../utils/messageUtils';
import { getEmailDomain } from '../Background/backgroundUtils';
import { getPresentationIdFromUrl } from '../Background/manualModeUtils';
import { setInDatabase, addToDatabase, getDatabaseSnapshot } from '../Background/firebase';

export default class Helper {
  presentationUrl: string;

  setIsPresentationActive: (value: boolean) => void | undefined;

  connectToPresentation: () => void;

  handleKeyboardEvent: (event: KeyboardEvent) => void;

  incrementSlide: () => void;

  decrementSlide: () => void;

  playPauseVideo: () => void;

  toggleFullScreenVideo: () => void;

  rewindVideo: () => void;

  forwardVideo: () => void;

  checkPresentationStatus: () => void;

  constructor(
    presentationUrl: string,
    user: User | null,
    setIsPresentationActive: (value: boolean) => void | undefined
  ) {
    this.presentationUrl = presentationUrl;
    this.setIsPresentationActive = setIsPresentationActive;
    // console.log(`Helper created for ${presentationUrl}`);
    this.connectToPresentation = async () => {
      console.log('Shared Slides Clicker: Connecting to Presentation ', presentationUrl);
      const domain = getEmailDomain(user);
      const presentationId = getPresentationIdFromUrl(presentationUrl);
      const connectedOn = new Date().toString();
      console.log(
        `Shared Slides Clicker: Adding myself to controllers of presentation ${presentationId}`
      );
      const connectedUserKey = `${domain}/${presentationId}/users/${user?.uid}`;
      return setInDatabase(connectedUserKey, { email: user?.email, connectedOn })
        .then(() => {
          console.log(
            `Shared Slides Clicker: Successfully connected to presentation ${presentationId}`
          );
        })
        .catch((error) => {
          console.log(
            `Shared Slides Clicker: Failed to connect to presentation ${presentationId}`,
            error
          );
          // TODO: send browser notification of connection failure
          // sendBrowserNotification(
          //   'Error sharing control of presentation',
          //   `Check that the presenter has the extension installed, is signed-in, and has started the presentation. Also, you and the presenter must share the same email domain (e.g., acme.com)`
          // );
        });
    };

    this.checkPresentationStatus = async () => {
      const presentationId = getPresentationIdFromUrl(presentationUrl);
      const domain = getEmailDomain(user);
      const snapshot = await getDatabaseSnapshot(`${domain}/${presentationId}`);
      const isPresentationActive = snapshot?.exists() || false;
      console.log(
        `Shared Slides Clicker: Presentation ${presentationId} is ${
          isPresentationActive ? 'active' : 'inactive'
        }`
      );
      if (!isPresentationActive) {
        Sentry.captureMessage(`Failed to connect to presentation`);
      }
      setIsPresentationActive(isPresentationActive);
    };

    this.handleKeyboardEvent = (event: KeyboardEvent) => {
      // console.log(`handleKeyboardEvent`, event);
      // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code/code_values
      const { key, keyCode } = event;
      if (key === 'ArrowDown' || key === 'ArrowRight' || keyCode === 32) {
        this.incrementSlide();
      } else if (key === 'ArrowUp' || key === 'ArrowLeft') {
        this.decrementSlide();
      }
    };

    /*
      Firebase Realtime DB hierarchy is: $domain/$presentationId/
      Schema is {
        info: {initiatedBy, initiatedOn},
        commands: {$commandId: {action, user, timestamp}}
      }
    */
    function addSlidesCommand(action: string): void {
      const presentationId = getPresentationIdFromUrl(presentationUrl);
      const domain = getEmailDomain(user);
      addToDatabase(`${domain}/${presentationId}/commands`, {
        action,
        user: user?.displayName,
        timestamp: serverTimestamp(),
      }).catch((error) => {
        if (error.code && error.code === 'PERMISSION_DENIED') {
          console.warn(
            `Shared Slides Clicker: Failed to send command ${action} - can't locate presentation ${presentationId}`
          );
        } else {
          console.error(
            `Shared Slides Clicker: Failed to send command ${action} for presentation ${presentationId}`,
            error
          );
          Sentry.captureException(error, {
            extra: {
              errorMessage: `Failed to send command ${action} for presentation ${presentationId}`,
            },
          });
        }
        backgroundSendBrowserNotification(
          'Error sharing control of presentation',
          `Check that the presenter has the extension installed, is signed-in, and has started the presentation. Also, you and the presenter must share the same email domain (e.g., acme.com)`
        );
      });
    }

    this.incrementSlide = () => {
      console.log('Shared Slides Clicker: Incrementing slide');
      addSlidesCommand('right');
      this.checkPresentationStatus();
    };
    this.decrementSlide = () => {
      console.log('Shared Slides Clicker: Decrementing slide');
      addSlidesCommand('left');
      this.checkPresentationStatus();
    };
    this.playPauseVideo = () => {
      console.log('Shared Slides Clicker: Play/pause video');
      addSlidesCommand('playPauseVideo');
      this.checkPresentationStatus();
    };
    this.toggleFullScreenVideo = () => {
      console.log('Shared Slides Clicker: Toggle full screen video');
      addSlidesCommand('toggleFullScreenVideo');
      this.checkPresentationStatus();
    };
    this.rewindVideo = () => {
      console.log('Shared Slides Clicker: Rewind video');
      addSlidesCommand('rewindVideo10Sec');
      this.checkPresentationStatus();
    };
    this.forwardVideo = () => {
      console.log('Shared Slides Clicker: Fast forward video');
      addSlidesCommand('forwardVideo10Sec');
      this.checkPresentationStatus();
    };
  }
}
