import { useState, useEffect, useMemo } from 'react';
import { User } from 'firebase/auth';
import * as Sentry from '@sentry/browser';
import { backgroundCheckForReadiness, backgroundOpenOptionsPage } from '../utils/messageUtils';

import Helper from './MeetControlHelper';
import MeetControlPanel from '../MeetControlPanel';
import MeetSetup from '../MeetSetup';
import MeetToggleButton from '../MeetToggleButton';
import './styles.scss';
import { ReadyCheckResponseMessage } from '../@types/messages';

type MeetControlWrapperProps = {
  firebaseUser: User | null;
  disableKeyboardShortcuts?: boolean;
  inAutomaticMode: boolean;
  initialPresentationUrl?: string; // only Storybook uses initialPresentationUrl
};

/* Note: MeetControlWrapper is re-rendered by MeetContentScript whenever the Meet sidebar is opened or closed */
export const MeetControlWrapper = ({
  firebaseUser,
  disableKeyboardShortcuts = false,
  inAutomaticMode,
  initialPresentationUrl,
}: MeetControlWrapperProps): JSX.Element | null => {
  /* Start state variables set by handleReadinessCheckResponse */
  const [isLoading, setIsLoading] = useState(true);
  const [user, setUser] = useState(firebaseUser);
  const [isEnabled, setIsEnabled] = useState(true);
  /* End state variables set by handleReadinessCheckResponse */

  const [isExpanded, setIsExpanded] = useState(false);
  const [presentationUrl, setPresentationUrl] = useState(initialPresentationUrl);
  const [isPresentationActive, setIsPresentationActive] = useState(false);
  const [isUrlSaved, setIsUrlSaved] = useState(false);
  const [isHidden, setIsHidden] = useState(false);

  /* disableKeyboardShortcuts is controlled by MeetContentScript */
  const [keyboardShortcutsEnabled, setKeyboardShortcutsEnabled] = useState(false);

  const helper = useMemo(
    () => new Helper(presentationUrl || '', firebaseUser, setIsPresentationActive),
    [presentationUrl, setIsPresentationActive]
  );

  useEffect(() => {
    backgroundCheckForReadiness(handleReadinessCheckResponse, handleError);
  }, []);

  useEffect(() => {
    if (!isEnabled || !isUrlSaved || !isExpanded) return undefined;
    // helper will check Firebase (via the backgroundPage) and set isPresentationActive accordingly
    helper.connectToPresentation();
    if (!keyboardShortcutsEnabled || disableKeyboardShortcuts) return undefined;
    document.addEventListener('keyup', helper.handleKeyboardEvent);
    return () => {
      document.removeEventListener('keyup', helper.handleKeyboardEvent);
    };
  }, [
    isEnabled,
    isUrlSaved,
    isExpanded,
    keyboardShortcutsEnabled,
    helper,
    disableKeyboardShortcuts,
  ]);

  const handleReadinessCheckResponse = (message: ReadyCheckResponseMessage) => {
    console.debug('Shared Slides Clicker: Ready check response', message);
    if (message && message.success) {
      const { user: messageUser, isEnabled: messageIsEnabled, tabId } = message.response;
      setIsLoading(false);
      setUser(messageUser);
      /* Handling isEnabled is superfluous: MeetControlWrapper won't be injected if not enabled
         because we want to let users turn off the button by disabling our features
      */
      setIsEnabled(messageIsEnabled);
      console.log(
        `Shared Slides Clicker: User ${messageUser ? 'is' : 'is not'} logged in and extension is ${
          messageIsEnabled ? 'enabled' : 'disabled'
        } on tab ${tabId}`
      );
    } else {
      Sentry.captureMessage('Unable to get readiness status in MeetControlWrapper');
      console.error('Shared Slides Clicker: Unable to get readiness status');
    }
  };
  const handleError = (error: Error) => {
    Sentry.captureException(error, {
      extra: { errorMessage: 'MeetControlWrapper Error from Background' },
    });
    console.error('Shared Slides Clicker: Error from background:', error);
  };

  const toggleIsExpanded = () => {
    setIsExpanded(!isExpanded);
  };

  const renderNotLoggedIn = () => {
    const className = 'ssc_meetControlWrapper collapsed';
    const openOptionsPage = async () => backgroundOpenOptionsPage();
    return (
      <div className={className}>
        <MeetToggleButton onClickHandler={openOptionsPage} isSignedIn={!!firebaseUser} />
      </div>
    );
  };

  const renderContracted = () => {
    const className = `ssc_meetControlWrapper${isExpanded ? '' : ' collapsed'}`;
    return (
      <div className={className}>
        <MeetToggleButton onClickHandler={toggleIsExpanded} isSignedIn={!!firebaseUser} />
      </div>
    );
  };

  const renderExpanded = () => {
    if (isUrlSaved) {
      return (
        <div
          className={
            isPresentationActive
              ? 'ssc_meetControlWrapper ssc_meetControlWrapperWide'
              : 'ssc_meetControlWrapper ssc_meetControlWrapperWideMedium'
          }
        >
          <MeetControlPanel
            isPresentationActive={isPresentationActive}
            checkPresentationStatus={helper.checkPresentationStatus}
            backgroundOpenOptionsPage={backgroundOpenOptionsPage}
            toggleIsExpanded={toggleIsExpanded}
            setIsUrlSaved={setIsUrlSaved}
            incrementSlide={helper.incrementSlide}
            decrementSlide={helper.decrementSlide}
            playPauseVideo={helper.playPauseVideo}
            rewindVideo={helper.rewindVideo}
            forwardVideo={helper.forwardVideo}
          />
        </div>
      );
    }
    return (
      <div className="ssc_meetControlWrapper ssc_meetControlWrapperWideTall">
        <MeetSetup
          presentationUrl={presentationUrl}
          setPresentationUrl={setPresentationUrl}
          checkPresentationStatus={helper.checkPresentationStatus}
          toggleIsExpanded={toggleIsExpanded}
          setIsUrlSaved={setIsUrlSaved}
          setIsHidden={setIsHidden}
          keyboardShortcutsEnabled={keyboardShortcutsEnabled}
          setKeyboardShortcutsEnabled={setKeyboardShortcutsEnabled}
          inAutomaticMode={inAutomaticMode}
        />
      </div>
    );
  };

  if (isLoading || isHidden) {
    return null;
  }
  if (!user || !isEnabled) {
    return renderNotLoggedIn();
  }
  if (isExpanded) {
    return renderExpanded();
  }
  return renderContracted();
};
