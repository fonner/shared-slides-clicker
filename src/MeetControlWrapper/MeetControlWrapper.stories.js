/* eslint-disable */
import React from 'react';

import { MeetControlWrapper } from './MeetControlWrapper.tsx';

export default {
  title: 'Components/Meet Control Wrapper',
  component: MeetControlWrapper,
  argTypes: {},
  decorators: [
    (Story) => (
      <div style={{ backgroundColor: 'black', height: '400px' }}>
        <Story />
      </div>
    ),
  ],
};

const Template = (args) => <MeetControlWrapper {...args} />;

export const KeyboardShortcutsDisabled = Template.bind({});
KeyboardShortcutsDisabled.args = {
  initialPresentationUrl: 'https://docs.google.com/presentation/d/1234567890abcdefg/edit',
  disableKeyboardShortcuts: true,
  inAutomaticMode: false,
};

export const KeyboardShortcutsEnabled = Template.bind({});
KeyboardShortcutsEnabled.args = {
  initialPresentationUrl: 'https://docs.google.com/presentation/d/1234567890abcdefg/edit',
  disableKeyboardShortcuts: false,
  inAutomaticMode: false,
};
