import * as Sentry from '@sentry/browser';
import OptionToggle from '../OptionToggle';
import { backgroundToggleMode } from '../utils/messageUtils';
import './styles.scss';
import { ToggleManualModeResponseMessage } from '../@types/messages';

interface ModeOptionToggleProps {
  inAutomaticMode: boolean;
  setInAutomaticMode: (inAutomaticMode: boolean) => void;
  setShowRefreshMessage: (showRefreshMessage: boolean) => void;
  inPopup?: boolean;
  isDisabled?: boolean;
}

export const ModeOptionToggle = ({
  inAutomaticMode = true,
  setInAutomaticMode,
  setShowRefreshMessage,
  isDisabled = false,
}: ModeOptionToggleProps): JSX.Element => {
  const handleModeToggleResponse = (message: ToggleManualModeResponseMessage) => {
    if (message && message.success) {
      const { isInAutomaticMode } = message.response;
      setInAutomaticMode(isInAutomaticMode);
      setShowRefreshMessage(true);
      console.log(`Extension automatic mode is ${isInAutomaticMode ? 'on' : 'off'}`);
    } else {
      Sentry.captureMessage('Unable to get mode status');
      console.error('Unable to get mode status');
    }
  };
  const handleError = (error: Error) => {
    Sentry.captureException(error, {
      extra: { errorMessage: 'ModeOptionsToggle Error from Background' },
    });
    console.error(`Error from background script: ${error}`);
  };
  const toggleMode = async () => {
    return backgroundToggleMode(handleModeToggleResponse, handleError);
  };

  return (
    <OptionToggle
      label="Automatic Mode"
      enabledMessage="When automatic mode is on, control sharing will be enabled automatically for every presentation."
      disabledMessage="When automatic mode is off, you must use the extension button in the toolbar to share control of a presentation."
      isEnabled={inAutomaticMode}
      doBackgroundToggle={toggleMode}
      isDisabled={isDisabled}
    />
  );
};
