import * as React from 'react';
import { Tabs } from 'webextension-polyfill-ts';
import { handlePresentationManualButtonClick } from '../Background/manualModeUtils';
import { SLIDE_SHARE_STATUS } from '../utils/messageConstants';
import TextAndCopyButton from '../TextAndCopyButton';
import { PopupButton } from './PopupButton';
import './styles.scss';

interface SlidesTabPopupProps {
  isEnabled: boolean;
  inAutomaticMode: boolean;
  currentTab: Tabs.Tab | undefined;
  status: SLIDE_SHARE_STATUS | undefined;
  alreadyClicked: boolean;
  setAlreadyClicked: (alreadyClicked: boolean | undefined) => void;
}

export const SlidesTabPopup = ({
  isEnabled,
  inAutomaticMode,
  currentTab,
  alreadyClicked,
  setAlreadyClicked,
  status,
}: SlidesTabPopupProps): JSX.Element | null => {
  return (
    <div className="ssc_popup">
      {isEnabled && !inAutomaticMode && (
        <SlidesButton
          currentTab={currentTab}
          alreadyClicked={alreadyClicked}
          setAlreadyClicked={setAlreadyClicked}
        />
      )}
      {isEnabled && inAutomaticMode && <SlidesMessage status={status} currentTab={currentTab} />}
    </div>
  );
};

interface SlidesButtonProps {
  currentTab: Tabs.Tab | undefined;
  alreadyClicked: boolean | undefined;
  setAlreadyClicked: (alreadyClicked: boolean | undefined) => void;
}
const SlidesButton = ({
  currentTab,
  alreadyClicked,
  setAlreadyClicked,
}: SlidesButtonProps): JSX.Element => {
  const handleButtonClick = () => {
    if (currentTab && currentTab.url) {
      const url = new URL(currentTab.url);
      const urlString = `${url.origin}${url.pathname}`;
      handlePresentationManualButtonClick(urlString, !alreadyClicked);
    }
    setAlreadyClicked(!alreadyClicked);
    // setTimeout(() => window.close(), 1500);
  };

  return (
    <div className="ssc_popup ssc_popupText">
      {alreadyClicked && (
        <React.Fragment>
          <p className="success">This presentation is being shared.</p>
          <p>
            Other participants with the Shared Slides Clicker extension should enter the URL below
            into their overlay in Google Meet in order to share control of your slides.
          </p>
          <TextAndCopyButton textToCopy={currentTab?.url} caption="URL" />
          <p>
            If the other presenters are not using the extension in Google Meet, they can use the Web
            Clicker at the URL below.
          </p>
          <TextAndCopyButton
            textToCopy={`https://fonner.gitlab.io/shared-slides-clicker/webclicker/?url=${encodeURI(
              currentTab?.url || ''
            )}`}
            caption="Web Clicker URL"
          />

          <p>Note: the slides must be in Slideshow mode for others to share control.</p>
        </React.Fragment>
      )}
      {!alreadyClicked && (
        <p>
          Click the button below to share control of this presentation with others in your Meet who
          are using the Shared Slides Clicker extension. <br />
          <br /> The presentation must be in Slideshow mode for others to share control.
        </p>
      )}

      <PopupButton
        onButtonClick={handleButtonClick}
        label={alreadyClicked ? 'Stop sharing this presentation' : 'Share this presentation'}
      />
    </div>
  );
};

interface SlidesMessageProps {
  currentTab: Tabs.Tab | undefined;
  status: SLIDE_SHARE_STATUS | undefined;
}

const SlidesMessage = ({ currentTab, status }: SlidesMessageProps): JSX.Element => {
  if (status === SLIDE_SHARE_STATUS.ERROR) {
    return (
      <div className="ssc_popupText">
        <p>
          An error occurred sharing your presentation 😭. Close your Slides browser tab, sign-out of
          the extension, sign-in again, then re-present.
        </p>
      </div>
    );
  }
  if (status === SLIDE_SHARE_STATUS.NOT_SHARED) {
    return (
      <div className="ssc_popupText">
        <p>
          To start sharing your presentation, click the Slideshow button in the top right corner of
          the Google Slides window.
        </p>
        <p>
          Once you click the Slideshow button, other presenters with this extension will be able to
          share control of your slides by entering the URL below into their Shared Slides Clicker
          overlay in Google Meet.
        </p>
        <TextAndCopyButton textToCopy={currentTab?.url} caption="Presentaiton URL" />
        <p>
          If the other presenters are not using the extension in Google Meet, they can use the Web
          Clicker at the URL below.
        </p>
        <TextAndCopyButton
          textToCopy={`https://fonner.gitlab.io/shared-slides-clicker/webclicker/?url=${encodeURI(
            currentTab?.url || ''
          )}`}
          caption="Web Clicker URL"
        />
        <br />
      </div>
    );
  }
  if (status === SLIDE_SHARE_STATUS.SHARED) {
    return (
      <>
        <div className="ssc_popupText">
          <p className="success">Your presentation is being shared 🎉</p>
          <p>
            Provide the Presentation URL below to other presenters with this extension to enter into
            Shared Slides Clicker overlay in Google Meet.
          </p>
          <TextAndCopyButton textToCopy={currentTab?.url} caption="Presentaiton URL" />
          <p>
            If the other presenters are not using the extension in Google Meet, they can use the Web
            Clicker at the URL below.
          </p>
          <TextAndCopyButton
            textToCopy={`https://fonner.gitlab.io/shared-slides-clicker/webclicker/?url=${encodeURI(
              currentTab?.url || ''
            )}`}
            caption="Web Clicker URL"
          />
          <br />
        </div>
      </>
    );
  }
  return (
    <div className="ssc_popupText">
      <p>
        The Shared Slides Clicker is not able to communicate with your slides 😱. Try refreshing
        your Slides tab or restarting your browser.
      </p>
    </div>
  );
};
