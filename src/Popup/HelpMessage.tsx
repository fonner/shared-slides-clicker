import { openWebPage } from './Popup';

export const HelpMessage = (): JSX.Element => (
  <div className="ssc_popupText">
    <p>
      This extension allows presenters to share control of their Google Slides presentations while
      screen-sharing to Google Meet.{' '}
    </p>
    <p>
      For more information, check out the&nbsp;
      <a href="#" onClick={() => openWebPage('https://fonner.gitlab.io/shared-slides-clicker/')}>
        extension homepage
      </a>
      .
    </p>
    <p>
      When in automatic mode, you don&apos;t need to use this popup window - everything is handled
      automatically.
    </p>
    <p>
      When in manual mode,{' '}
      <u>buttons will appear here when you are on a Google Meet or Google Slides tab</u>.
    </p>
    <p>
      Click the gear button above to change settings and see detailed instructions on how to use
      this extension.
    </p>
  </div>
);
