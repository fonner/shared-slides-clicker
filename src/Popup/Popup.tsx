import * as React from 'react';
import { browser, Tabs } from 'webextension-polyfill-ts';
import { SlidesShareStatusResponseMessage, TypedMessage } from '../@types/messages';
import { getManualButtonStatus, handleMeetManualButtonClick } from '../Background/manualModeUtils';
import MESSAGE_TYPE, { SLIDE_SHARE_STATUS } from '../utils/messageConstants';
import { HelpMessage } from './HelpMessage';
import { MeetTabPopup } from './MeetTabPopup';
import { SlidesTabPopup } from './SlidesTabPopup';
import './styles.scss';

export async function openWebPage(url: string): Promise<Tabs.Tab> {
  return browser.tabs.create({ url });
}

interface PopupProps {
  isEnabled: boolean;
  inAutomaticMode: boolean;
}

export const Popup = ({ isEnabled, inAutomaticMode }: PopupProps): JSX.Element | null => {
  const [currentTab, setCurrentTab] = React.useState<Tabs.Tab | undefined>(undefined);
  const [alreadyClicked, setAlreadyClicked] = React.useState<boolean | undefined>(undefined);

  const [slidesShareStatus, setSlidesShareStatus] = React.useState<SLIDE_SHARE_STATUS | undefined>(
    undefined
  );

  const getCurrentTab = async () => {
    const query = { active: true, currentWindow: true };
    const tabs = await browser.tabs.query(query);
    const curr = tabs[0];
    setCurrentTab(curr);
    return curr;
  };

  React.useEffect(() => {
    getCurrentTab()
      // Check status of current Presentation tab
      .then((tab) => {
        if (!tab.url?.startsWith('https://docs.google.com/presentation')) {
          console.log('Not a Slides tab', currentTab);
          return Promise.reject();
        }
        if (tab.id) {
          console.log('Checking for slides status', tab.id);
          const message: TypedMessage = { type: MESSAGE_TYPE.SLIDES_SHARE_STATUS };
          return browser.tabs.sendMessage(tab.id, message);
        }
        console.warn('Current tab not found');
        return Promise.reject();
      })
      .then((reply: SlidesShareStatusResponseMessage | null) => {
        if (!reply) {
          console.warn('Received empty reply');
          return;
        }
        const { status } = reply.response;
        console.log('Received response: ', reply);
        setSlidesShareStatus(status);
      })
      .catch(() => {
        /* Not a Presentation tab */
      });
  }, []);

  const wasManualButtonClicked = React.useCallback(async (tab) => {
    if (!tab || !tab.url) return;
    const {
      success,
      response: { wasClicked },
    } = await getManualButtonStatus(tab.url);
    if (!success) {
      console.warn('Shared Slides Clicker Popup: Unable to determine if manual button was clicked');
    }
    setAlreadyClicked(!!wasClicked);
  }, []);

  React.useEffect(() => {
    if (!currentTab) {
      return;
    }
    if (
      currentTab.url?.startsWith('https://meet.google.com') ||
      currentTab.url?.startsWith('https://docs.google.com/presentation')
    ) {
      wasManualButtonClicked(currentTab);
    } else {
      setAlreadyClicked(false);
    }
  }, [currentTab, wasManualButtonClicked]);

  const isSlidesTab = () =>
    currentTab && currentTab.url?.startsWith('https://docs.google.com/presentation');
  const isMeetTab = () => currentTab && currentTab.url?.startsWith('https://meet.google.com/');
  const isOtherTab = () => !isSlidesTab() && !isMeetTab();

  if (currentTab === undefined || alreadyClicked === undefined) return null;

  return (
    <section id="popup">
      <div className="ssc_popup ssc_popupHeader">
        <h2 style={{ marginLeft: '45px' }}>Shared Slides Clicker</h2>
        <button
          type="button"
          className="ssc_button secondary"
          style={{ margin: '10px' }}
          onClick={() => {
            return openWebPage('options.html');
          }}
        >
          <img
            src={browser.runtime.getURL('assets/icons/settings.svg')}
            height="18"
            width="18"
            alt="Options"
            title="Options"
          />
        </button>
      </div>

      <p className="ssc_popupText" style={{ color: 'red', textAlign: 'center' }}>
        The Shared Slide Clicker extension is no longer needed. Meet now{' '}
        <a
          href="https://workspaceupdates.googleblog.com/2023/04/co-present-google-slides-google-meet.html"
          target="_blank"
          rel="noreferrer noopener"
        >
          natively supports
        </a>{' '}
        sharing presentation control. Please{' '}
        <a
          href="#"
          onClick={(e) => {
            e.preventDefault();
            openWebPage('options.html#solong');
          }}
        >
          uninstall this extension
        </a>
        .
      </p>

      {(!isEnabled || isOtherTab()) && <HelpMessage />}

      {isSlidesTab() && (
        <SlidesTabPopup
          isEnabled={isEnabled}
          inAutomaticMode={inAutomaticMode}
          currentTab={currentTab}
          status={slidesShareStatus}
          alreadyClicked={alreadyClicked}
          setAlreadyClicked={setAlreadyClicked}
        />
      )}
      {isMeetTab() && (
        <MeetTabPopup
          isEnabled={isEnabled}
          inAutomaticMode={inAutomaticMode}
          currentTab={currentTab}
          alreadyClicked={alreadyClicked}
          setAlreadyClicked={setAlreadyClicked}
          handleMeetManualButtonClick={handleMeetManualButtonClick}
        />
      )}
      {isEnabled && !isOtherTab() && (
        <div className="ssc_popup">
          <span style={{ margin: '0 0 10px 0' }}>
            <a
              href="#"
              onClick={(e) => {
                e.preventDefault();
                openWebPage('options.html#troubleshooting');
              }}
            >
              Having Trouble?
            </a>
          </span>
        </div>
      )}
      <div className="ssc_popup ssc_popupFooter">{getSettingsText(isEnabled, inAutomaticMode)}</div>
    </section>
  );
};

const getSettingsText = (isEnabled: boolean, inAutomaticMode: boolean) => {
  return (
    <p className="ssc_popupText">
      <strong>
        Shared Slides Clicker is currently{' '}
        <span className={isEnabled ? 'positive' : 'negative'}>
          {isEnabled ? 'enabled' : 'disabled.'}
        </span>{' '}
        {isEnabled && (
          <React.Fragment>
            and in{' '}
            <span className={inAutomaticMode ? 'positive' : 'negative'}>
              {inAutomaticMode ? 'automatic' : 'manual'}
            </span>{' '}
            mode.
          </React.Fragment>
        )}
      </strong>
    </p>
  );
};
