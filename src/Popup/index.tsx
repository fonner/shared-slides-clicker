import ReactDOM from 'react-dom';
import { browser } from 'webextension-polyfill-ts';
import * as Sentry from '@sentry/browser';
import { Popup } from './Popup';

import { backgroundCheckForReadiness } from '../utils/messageUtils';
import { ReadyCheckResponseMessage } from '../@types/messages';

const openOptions = () => {
  browser.tabs.create({ url: 'options.html' });
};

const handleReadinessCheckResponse = (message: ReadyCheckResponseMessage) => {
  if (message && message.success) {
    const { user, isEnabled, inAutomaticMode } = message.response;
    if (!user) {
      openOptions();
    }
    ReactDOM.render(
      <Popup isEnabled={isEnabled} inAutomaticMode={inAutomaticMode} />,
      document.getElementById('popup-root')
    );
  } else {
    openOptions();
  }
};
const handleError = (error: Error) => {
  Sentry.captureException(error, { extra: { errorMessage: 'Popup Page Error from Background' } });
  console.error(`Shared Slides Clicker: Error`, error);
};

backgroundCheckForReadiness(handleReadinessCheckResponse, handleError);
