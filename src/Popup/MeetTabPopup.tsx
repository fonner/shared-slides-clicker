import * as React from 'react';
import { Tabs } from 'webextension-polyfill-ts';
import { PopupButton } from './PopupButton';
import './styles.scss';

interface MeetTabPopupProps {
  isEnabled: boolean;
  inAutomaticMode: boolean;
  currentTab: Tabs.Tab | undefined;
  alreadyClicked: boolean;
  setAlreadyClicked: (alreadyClicked: boolean | undefined) => void;
  handleMeetManualButtonClick: (meetUrl: string, startSharing: boolean) => void;
}

export const MeetTabPopup = ({
  isEnabled,
  inAutomaticMode,
  currentTab,
  alreadyClicked,
  setAlreadyClicked,
  handleMeetManualButtonClick,
}: MeetTabPopupProps): JSX.Element | null => {
  return (
    <>
      {isEnabled && !inAutomaticMode && (
        <MeetButton
          currentTab={currentTab}
          alreadyClicked={alreadyClicked}
          setAlreadyClicked={setAlreadyClicked}
          handleMeetManualButtonClick={handleMeetManualButtonClick}
        />
      )}
      {isEnabled && inAutomaticMode && (
        <div className="ssc_popupText">
          <p>In automatic mode, you don&apos;t need to use this popup window. </p>
          <p>
            Use the Shared Slides Clicker overlay in the Google Meet window to enter the
            presentation URL and start sharing control.{' '}
          </p>
          <p>
            Note: the Shared Slides Clicker overlay will say &quot;Waiting for presentation&quot;
            until the presenter puts the slides into Slideshow mode.
          </p>
        </div>
      )}
    </>
  );
};

interface MeetButtonProps {
  currentTab: Tabs.Tab | undefined;
  alreadyClicked: boolean | undefined;
  setAlreadyClicked: (alreadyClicked: boolean | undefined) => void;
  handleMeetManualButtonClick: (meetUrl: string, startSharing: boolean) => void;
}
const MeetButton = ({
  currentTab,
  alreadyClicked,
  setAlreadyClicked,
  handleMeetManualButtonClick,
}: MeetButtonProps): JSX.Element => {
  const handleButtonClick = () => {
    if (currentTab && currentTab.url) {
      const url = new URL(currentTab.url);
      const urlString = `${url.origin}${url.pathname}`;
      handleMeetManualButtonClick(urlString, !alreadyClicked);
    }
    setAlreadyClicked(!alreadyClicked);
    // setTimeout(() => window.close(), 1500);
  };
  return (
    <div className="ssc_popup ssc_popupText">
      {!alreadyClicked && (
        <p>
          Click the button below to add the Shared Slides Clicker overlay to the top of Google Meet.
          Then click the overlay to start controlling a presentation.
        </p>
      )}
      {alreadyClicked && (
        <React.Fragment>
          <p className="success">Overlay added to Google Meet!</p>
          <p>Click the overlay at the top of Google Meet to start controlling a presentation.</p>
        </React.Fragment>
      )}
      <PopupButton
        onButtonClick={handleButtonClick}
        label={alreadyClicked ? 'Remove overlay' : 'Add control overlay'}
      />
    </div>
  );
};
