import { browser } from 'webextension-polyfill-ts';

interface PopupButtonProps {
  onButtonClick: () => void;
  label: string;
}
export const PopupButton = ({ onButtonClick, label }: PopupButtonProps): JSX.Element => {
  return (
    <button type="button" className="ssc_button" onClick={onButtonClick}>
      <img
        src={browser.runtime.getURL('assets/icons/remote-white.svg')}
        height="24"
        width="24"
        alt="icon"
        title={label}
      />
      <p>{label}</p>
    </button>
  );
};
