import React, { useState, useRef } from 'react';
import { browser } from 'webextension-polyfill-ts';
import { backgroundOpenOptionsPage } from '../utils/messageUtils';
import './styles.scss';

interface MeetSetupProps {
  presentationUrl?: string;
  setPresentationUrl: (url: string) => void;
  checkPresentationStatus: () => void;
  toggleIsExpanded: () => void;
  setIsUrlSaved: (isSaved: boolean) => void;
  setIsHidden: (isHidden: boolean) => void;
  keyboardShortcutsEnabled: boolean;
  setKeyboardShortcutsEnabled: (enabled: boolean) => void;
  inAutomaticMode: boolean;
}

export const MeetSetup = ({
  presentationUrl = '',
  setPresentationUrl,
  checkPresentationStatus,
  toggleIsExpanded,
  setIsUrlSaved,
  setIsHidden,
  keyboardShortcutsEnabled,
  setKeyboardShortcutsEnabled,
  inAutomaticMode,
}: MeetSetupProps): JSX.Element => {
  const [validation, setValidation] = useState<string | null>(null);
  const [showHideMessage, setShowHideMessage] = useState(false);
  const isAlreadyConfigured = useRef(!!presentationUrl);

  const openOptionsPage = async () => {
    return backgroundOpenOptionsPage();
  };
  const openOptionsPageToSection = async (section: string) => {
    return backgroundOpenOptionsPage(section);
  };
  const saveUrl = () => {
    const pathPieces = presentationUrl.split('/');
    if (pathPieces.length >= 6) {
      console.log('Shared Slides Clicker: Saving url:', presentationUrl);
      setIsUrlSaved(true);
      setValidation(null);
      checkPresentationStatus();
    } else {
      setValidation('Invalid Google Slides URL');
    }
  };
  const handlePresUrlChange = (event: React.ChangeEvent) => {
    const key = (event.target as HTMLInputElement).value;
    setPresentationUrl(key);
  };
  const handleKeyPress = (event: React.KeyboardEvent) => {
    if (event.key === 'Enter') {
      saveUrl();
    }
  };
  const toggleKeyboardEvents = () => {
    const value = !keyboardShortcutsEnabled;
    setKeyboardShortcutsEnabled(value);
  };
  const hideItAll = () => {
    setIsHidden(true);
  };
  const cancelOrDisconnect = () => {
    isAlreadyConfigured.current = false;
    setPresentationUrl('');
    toggleIsExpanded();
  };

  const renderCheckbox = () => {
    return (
      <label htmlFor="keyboardShortcuts" style={{ marginBottom: '5px' }}>
        <label
          className="ssc_switch"
          title={`Keyboard shortcuts are ${
            keyboardShortcutsEnabled ? 'enabled' : 'disabled'
          }. Click to ${keyboardShortcutsEnabled ? 'disable' : 'enable'}.`}
        >
          <input
            type="checkbox"
            checked={keyboardShortcutsEnabled}
            onChange={toggleKeyboardEvents}
          />
          <span className="ssc_slider" />
        </label>
        &nbsp;Keyboard Shortcuts&nbsp;
        <img
          src={browser.runtime.getURL('assets/icons/help-circle.svg')}
          alt="Keyboard shortcut help"
          title="When keyboard shortcuts are enabled, you can use the down or right arrow keys or the spacebar to go to the next slide and use the up or left arrow keys to go to previous slide."
        />
      </label>
    );
  };

  return (
    <div className="ssc_meetSetup ssc_horizontallyCentered">
      <p style={{ color: 'red', textAlign: 'center' }}>
        The Shared Slide Clicker extension is no longer needed. Meet now{' '}
        <a
          href="https://workspaceupdates.googleblog.com/2023/04/co-present-google-slides-google-meet.html"
          target="_blank"
          rel="noreferrer noopener"
        >
          natively supports
        </a>{' '}
        sharing presentation control.{' '}
        <strong>
          Please{' '}
          <a href="#" onClick={() => openOptionsPageToSection('solong')}>
            uninstall this extension
          </a>
          .
        </strong>
      </p>
      <label htmlFor="presUrl">
        <p className="ssc_header">Google Slides URL:</p>
      </label>
      <textarea
        id="presUrl"
        name="presUrl"
        value={presentationUrl}
        rows={3}
        onChange={handlePresUrlChange}
        onKeyPress={handleKeyPress}
      />

      {validation && <p className="ssc_validation">{validation}</p>}
      <p className="ssc_smallFont">
        Enter the URL of the Google Slides being presented, click Start, and then use the buttons to
        share control of the presentation.
      </p>
      {renderCheckbox()}
      <div
        className="ssc_horizontallyCentered"
        style={{ marginTop: !validation ? '10px' : '0px' }}
        onMouseLeave={() => setShowHideMessage(false)}
      >
        <div className="ssc_verticallyCentered">
          <button
            type="button"
            className="ssc_button"
            onMouseEnter={() => setShowHideMessage(false)}
            onClick={saveUrl}
            style={{ width: '100px' }}
            title="Start the Shared Slides Clicker"
          >
            Connect
          </button>
          <button
            type="button"
            className="ssc_button secondary"
            onMouseEnter={() => setShowHideMessage(false)}
            onClick={cancelOrDisconnect}
            style={{ width: '100px' }}
            title="Cancel configuring the Shared Slides Clicker"
          >
            {isAlreadyConfigured.current ? 'Disconnect' : 'Cancel'}
          </button>
          <button
            type="button"
            className="ssc_button secondary"
            onClick={hideItAll}
            onMouseEnter={() => setShowHideMessage(true)}
            style={{ width: '100px' }}
            title="Hide the Shared Slides Clicker button (refresh the Meet window to unhide)"
          >
            Hide
          </button>
        </div>
        {!showHideMessage && (
          <div className="ssc_havingTrouble" style={{ marginTop: '8px' }}>
            <a
              href="#"
              onClick={(e) => {
                e.preventDefault();
                openOptionsPageToSection('troubleshooting');
              }}
            >
              Having Trouble?
            </a>
          </div>
        )}
        {showHideMessage && inAutomaticMode && (
          <div className="ssc_hideMessage">
            If you frequently hide this overlay, <br />
            consider{' '}
            <span
              className="ssc_turnOffLink"
              onClick={openOptionsPage}
              onKeyPress={openOptionsPage}
            >
              turning off Automatic Mode
            </span>
            .
          </div>
        )}
      </div>
    </div>
  );
};
