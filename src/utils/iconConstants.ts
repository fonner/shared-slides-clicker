export const READY_ICONS = {
  16: 'assets/icons/favicon-16.png',
  32: 'assets/icons/favicon-32.png',
  48: 'assets/icons/favicon-48.png',
  128: 'assets/icons/favicon-128.png',
};
export const NOT_READY_ICONS = {
  16: 'assets/icons/favicon-warning-16.png',
  32: 'assets/icons/favicon-warning-32.png',
  48: 'assets/icons/favicon-warning-48.png',
  128: 'assets/icons/favicon-warning-128.png',
};
export const PRESENTING_ICONS = {
  16: 'assets/icons/favicon-greencircle-16.png',
  32: 'assets/icons/favicon-greencircle-32.png',
  48: 'assets/icons/favicon-greencircle-48.png',
  128: 'assets/icons/favicon-greencircle-128.png',
};
