/* eslint-disable no-unused-vars */
import { browser } from 'webextension-polyfill-ts';
import MESSAGE_TYPE from './messageConstants';
import {
  AuthChangedResponseMessage,
  BrowserNotifyRequestMessage,
  OpenOptionsPageRequestMessage,
  ReadyCheckResponseMessage,
  ToggleEnabledResponseMessage,
  ToggleManualModeResponseMessage,
  TypedMessage,
  UnshareRequestMessage,
  UpdateIconRequestMessage,
} from '../@types/messages';

/* *************************** Background Calls ***************************

   This file is just a utility method that makes it easier for content scripts and
   injected React components to send messages to the background script.

*/

/**
 * Sends a message to the background script to check if user is signed in and SSC enabled
 * @param handleResponse
 * @param handleError
 */
export const backgroundCheckForReadiness = async (
  handleResponse: (m: ReadyCheckResponseMessage) => void,
  handleError: (e: Error) => void
): Promise<void> => {
  console.log('Shared Slides Clicker: Checking for Readiness');
  const message: TypedMessage = { type: MESSAGE_TYPE.READY_CHECK };
  const sending = browser.runtime.sendMessage(message);
  return sending.then(handleResponse, handleError);
};

/**
 * Sends a message to the background script to toggle whether SSC is enabled
 * @param handleResponse
 * @param handleError
 */
export const backgroundToggleEnabled = async (
  handleResponse: (m: ToggleEnabledResponseMessage) => void,
  handleError: (e: Error) => void
): Promise<void> => {
  const message: TypedMessage = { type: MESSAGE_TYPE.TOGGLE_ENABLED };
  const sending = browser.runtime.sendMessage(message);
  return sending.then(handleResponse, handleError);
};

/**
 * Sends a message to the background script to toggle Manual or Automatic mode
 * @param handleResponse
 * @param handleError
 */
export const backgroundToggleMode = async (
  handleResponse: (m: ToggleManualModeResponseMessage) => void,
  handleError: (e: Error) => void
): Promise<void> => {
  const message: TypedMessage = { type: MESSAGE_TYPE.TOGGLE_MODE };
  const sending = browser.runtime.sendMessage(message);
  return sending.then(handleResponse, handleError);
};

/**
 * Sends a message to the background script to sign in
 * @param handleResponse
 * @param handleError
 */
export const backgroundSignIn = async (
  handleResponse: (m: AuthChangedResponseMessage) => void,
  handleError: (e: Error) => void
): Promise<void> => {
  const message: TypedMessage = { type: MESSAGE_TYPE.GOOGLE_SIGN_IN };
  const sending = browser.runtime.sendMessage(message);
  return sending.then(handleResponse, handleError);
};

/**
 * Sends a message to the background script to sign out
 * @param handleResponse
 * @param handleError
 */
export const backgroundSignOut = async (
  handleResponse: (m: AuthChangedResponseMessage) => void,
  handleError: (e: Error) => void
): Promise<void> => {
  const message: TypedMessage = { type: MESSAGE_TYPE.GOOGLE_SIGN_OUT };
  const sending = browser.runtime.sendMessage(message);
  return sending.then(handleResponse, handleError);
};

export const backgroundUpdateIcon = async (isPresenting: boolean): Promise<void> => {
  const message: UpdateIconRequestMessage = {
    type: MESSAGE_TYPE.UPDATE_ICON,
    isPresenting,
  };
  return browser.runtime.sendMessage(message);
};

export const backgroundSendBrowserNotification = async (
  title: string,
  message: string
): Promise<void> => {
  const msg: BrowserNotifyRequestMessage = {
    type: MESSAGE_TYPE.BROWSER_NOTIFY,
    title,
    message,
  };
  return browser.runtime.sendMessage(msg);
};

/**
 * Sends a message to the background script check whether the presentation is being shared
 * @param presentationUrl
 * @param handleResponse
 * @param handleError
 */
// export const backgroundCheckPresentationStatus = async (
//   presentationUrl: string,
//   handleResponse: (arg: PresentationCheckResponseMessage) => void,
//   handleError: (error: Error) => void
// ): Promise<void> => {
//   const message: PresentationCheckRequestMessage = {
//     type: MESSAGE_TYPE.CHECK_PRESENTATION,
//     presentationUrl,
//   };
//   const sending = browser.runtime.sendMessage(message);
//   return sending.then(handleResponse, handleError);
// };

/**
 * Sends a message to the background script to stop active presentation sharing
 * @param presentationId
 * @param handleResponse
 * @param handleError
 */
// export const backgroundStopPresentation = async (
//   presentationId: string,
//   handleResponse: (m: JustSuccessResponseMessage) => void,
//   handleError: (e: Error) => void
// ): Promise<void> => {
//   const message: PresentationControlRequestMessage = {
//     type: MESSAGE_TYPE.STOP_PRESENTATION,
//     presentationId,
//   };
//   const sending = browser.runtime.sendMessage(message);
//   return sending.then(handleResponse, handleError);
// };

/**
 * Sends a message from Slides to the background script to signal
 * a slide command was processed and should be removed
 * @param presentationId
 * @param snapshotKey
 * @param action
 * @param name
 * @param tabId
 */
// export const backgroundHandleSlideActionCommand = async (
//   presentationId: string,
//   snapshotKey: string,
//   action: string,
//   name: string,
//   tabId: number | null
// ): Promise<void> => {
//   const message: HandleSlideActionRequestMessage = {
//     type: MESSAGE_TYPE.HANDLE_SLIDE_ACTION,
//     presentationId,
//     snapshotKey,
//     action,
//     name,
//     tabId,
//   };
//   return browser.runtime.sendMessage(message);
// };

/**
 * Sends a message from Meet to the background script to signal a new slide command
 * @param presentationUrl
 * @param action
 */
// export const backgroundAddSlidesCommand = async (
//   presentationUrl: string,
//   action: string
// ): Promise<void> => {
//   const message: AddActionRequestMessage = {
//     type: MESSAGE_TYPE.ADD_SLIDE_ACTION,
//     presentationUrl,
//     action,
//   };
//   return browser.runtime.sendMessage(message);
// };

/**
 * Sends a message to the background script to open the Options page because
 * content scripts can't direclty open the options page
 * @param section
 */
export const backgroundOpenOptionsPage = async (section?: string): Promise<void> => {
  const message: OpenOptionsPageRequestMessage = { type: MESSAGE_TYPE.OPEN_OPTIONS, section };
  return browser.runtime.sendMessage(message);
};

/* Used when unloading Presentation content script or handling manual button clicks from the Popup */
export const backgroundUnsharePresentation = async (presentationUrl: string): Promise<void> => {
  const message: UnshareRequestMessage = {
    type: MESSAGE_TYPE.UNSHARE_PRESENTATION,
    presentationUrl,
  };
  return browser.runtime.sendMessage(message);
};

/* Used when unloading Meet content script */
export const backgroundUnshareControl = async (meetUrl: string): Promise<void> => {
  const message: UnshareRequestMessage = { type: MESSAGE_TYPE.UNSHARE_CONTROL, meetUrl };
  return browser.runtime.sendMessage(message);
};
