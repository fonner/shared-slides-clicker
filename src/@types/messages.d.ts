import { User } from 'firebase/auth';
import MESSAGE_TYPE, { SLIDE_SHARE_STATUS } from '../utils/messageConstants';

// TODO: go through and remove message types we no longer need

/* *** Message Requests *** */

interface TypedMessage {
  type: MESSAGE_TYPE | 'SIGN_CONNECT';
}
interface PresentationCheckRequestMessage extends TypedMessage {
  presentationUrl: string;
}

interface PresentationControlRequestMessage extends TypedMessage {
  presentationId: string;
  startSharing: boolean;
}

interface PresentationStopSharingRequestMessage extends TypedMessage {
  presentationId: string;
  wasRemotelyControlled: boolean;
}
interface MeetControlRequestMessage extends TypedMessage {
  meetUrl: string;
  startControl: boolean;
}

interface AddActionRequestMessage extends TypedMessage {
  presentationUrl: string;
  action: string;
}
interface HandleSlideActionRequestMessage extends TypedMessage {
  presentationId: string;
  snapshotKey: string;
  action: string;
  name: string;
  tabId?: number | null;
}

interface ManualShareActionRequestMessage extends TypedMessage {
  presentationId: string;
  startSharing: boolean;
}

interface UnshareRequestMessage extends TypedMessage {
  meetUrl?: string;
  presentationUrl?: string;
}

interface OpenOptionsPageRequestMessage extends TypedMessage {
  section?: string;
}

interface UpdateIconRequestMessage extends TypedMessage {
  isPresenting: boolean;
}
interface BrowserNotifyRequestMessage extends TypedMessage {
  title: string;
  message: string;
}

type AllRequestMessages = TypedMessage &
  PresentationCheckRequestMessage &
  PresentationControlRequestMessage &
  PresentationStopSharingRequestMessage &
  MeetControlRequestMessage &
  AddActionRequestMessage &
  HandleSlideActionRequestMessage &
  ManualShareActionRequestMessage &
  OpenOptionsPageRequestMessage &
  UnshareRequestMessage &
  BrowserNotifyRequestMessage &
  UpdateIconRequestMessage;

/* *** Message Responses *** */

interface JustSuccessResponseMessage extends TypedMessage {
  success: boolean;
}

interface ReadyCheckResponseMessage extends JustSuccessResponseMessage {
  response: {
    user: User | null;
    tabId: number | null;
    isEnabled: boolean;
    inAutomaticMode: boolean;
  };
}

interface AuthChangedResponseMessage extends JustSuccessResponseMessage {
  response: User | string;
}

interface ToggleEnabledResponseMessage extends JustSuccessResponseMessage {
  response: { isEnabled: boolean };
}

interface PresentationCheckResponseMessage extends JustSuccessResponseMessage {
  response?: {
    isPresentationActive: boolean;
  };
}

interface ToggleManualModeResponseMessage extends JustSuccessResponseMessage {
  response: { isInAutomaticMode: boolean };
}

interface ManualModeStatusResponseMessage extends JustSuccessResponseMessage {
  response: { wasClicked: boolean };
}

interface BrowserNotifyResponseMessage extends JustSuccessResponseMessage {
  response: { notificationId: string };
}

interface SlidesShareStatusResponseMessage extends JustSuccessResponseMessage {
  response: { status: SLIDE_SHARE_STATUS };
}

type AllResponseMessages =
  | JustSuccessResponseMessage
  | ReadyCheckResponseMessage
  | AuthChangedResponseMessage
  | ToggleEnabledResponseMessage
  | PresentationCheckResponseMessage
  | ToggleManualModeResponseMessage
  | ManualModeStatusResponseMessage
  | BrowserNotifyResponseMessage
  | SlidesShareStatusResponseMessage;
