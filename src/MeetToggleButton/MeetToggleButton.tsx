import { MouseEvent, KeyboardEvent } from 'react';
import { browser } from 'webextension-polyfill-ts';
import './styles.scss';

interface MeetToggleButtonProps {
  onClickHandler: (event: MouseEvent | KeyboardEvent) => void;
  isSignedIn: boolean;
}

export const MeetToggleButton = ({
  onClickHandler,
  isSignedIn,
}: MeetToggleButtonProps): JSX.Element => (
  <span id="ssc_toggleButtonController" onClick={onClickHandler} onKeyPress={onClickHandler}>
    <img
      src={browser.runtime.getURL('assets/icons/remote-black.svg')}
      width="32px"
      height="32px"
      alt="Remote"
      title={
        isSignedIn ? 'Expand presentation controls' : 'Please sign in to use Shared Slides Clicker'
      }
    />
  </span>
);
