import { useState, useEffect, MouseEventHandler, Fragment } from 'react';
import { browser } from 'webextension-polyfill-ts';
import * as Sentry from '@sentry/browser';
import { User } from 'firebase/auth';
import ModeOptionToggle from '../ModeOptionToggle';
import EnabledOptionToggle from '../EnabledOptionToggle';
import SignInButton from '../SignInButton';

import { backgroundCheckForReadiness } from '../utils/messageUtils';
import './styles.scss';
import { ReadyCheckResponseMessage } from '../@types/messages';

export const Options = (): JSX.Element => {
  const [isLoading, setIsLoading] = useState(true);
  const [user, setUser] = useState<User | null>(null);
  const [showRefreshMessage, setShowRefreshMessage] = useState<boolean>(false);
  const [isEnabled, setIsEnabled] = useState<boolean>(true);
  const [inAutomaticMode, setInAutomaticMode] = useState<boolean>(true);
  const [wasCopied, setWasCopied] = useState<boolean>(false);

  const handleError = (error: Error) => {
    Sentry.captureException(error, {
      extra: { errorMessage: 'Options Page Error from Background' },
    });
    console.error(`Error from background script: ${error}`);
  };

  useEffect(() => {
    backgroundCheckForReadiness(handleReadinessCheckResponse, handleError);
    setTimeout(() => {
      scrollToHash(window.location);
    }, 250);
  }, []);

  const scrollToHash = (location: Location) => {
    if (location.hash) {
      const hash = location.hash.slice(1);
      const hashElementList = document.getElementsByName(hash);
      if (hashElementList && hashElementList.length > 0) {
        const hashElement = hashElementList[0];
        const rect = hashElement.getBoundingClientRect();
        hashElement.scrollIntoView({ behavior: 'smooth' });
        if (rect.y > 700) {
          // we've scrolled a lot so draw attention to the target element
          hashElement.classList.add('ssc_flash');
        }
      }
    }
  };

  const handleReadinessCheckResponse = (message: ReadyCheckResponseMessage) => {
    if (message && message.success) {
      const {
        user: messageUser,
        isEnabled: messageIsEnabled,
        inAutomaticMode: messageInAutomaticMode,
      } = message.response;
      setIsLoading(false);
      setUser(messageUser);
      setIsEnabled(messageIsEnabled);
      setInAutomaticMode(messageInAutomaticMode);
      console.log(
        `Readiness: User ${messageUser ? 'is' : 'is not'} logged in and extension is ${
          messageIsEnabled ? 'enabled' : 'disabled'
        } and ${messageInAutomaticMode ? 'in' : 'not in'} automatic mode`
      );
    } else {
      Sentry.captureMessage('Unable to get readiness status');
      console.error('Unable to get readiness status');
    }
  };

  const putExtensionUrlInClipboard = () => {
    navigator.clipboard.writeText('https://fonner.gitlab.io/shared-slides-clicker/');
    setWasCopied(true);
  };

  return (
    <div className="ssc_options">
      <h2>SHARED SLIDES CLICKER v{process.env.APP_VERSION}</h2>
      <h3 className="subtitle">For Google Slides and Meet</h3>
      <div className="ssc_fullWidthText" style={{ width: '500px' }}>
        <p>
          This extension allows presenters to share control of their Google Slides presentations
          while screen-sharing to Google Meet.
        </p>
        <p>
          For more information, check out the&nbsp;
          <a
            href="https://fonner.gitlab.io/shared-slides-clicker/"
            target="_blank"
            rel="noreferrer noopener"
          >
            extension homepage
          </a>
          &nbsp;as well as the&nbsp;
          <a
            href="https://fonner.gitlab.io/shared-slides-clicker/privacy/"
            target="_blank"
            rel="noreferrer noopener"
          >
            privacy policy
          </a>
          &nbsp;and&nbsp;
          <a
            href="https://fonner.gitlab.io/shared-slides-clicker/terms/"
            target="_blank"
            rel="noreferrer noopener"
          >
            terms and conditions
          </a>
          .
        </p>
      </div>
      {!isLoading && !user && (
        <p>
          <strong className="color primary">
            Please authenticate using your Google Account first
          </strong>
        </p>
      )}
      <div className="ssc_container">
        <SignInButton
          user={user}
          setUser={setUser}
          isLoading={isLoading}
          setShowRefreshMessage={setShowRefreshMessage}
        />
      </div>
      <EnabledOptionToggle
        isEnabled={isEnabled}
        setIsEnabled={setIsEnabled}
        setShowRefreshMessage={setShowRefreshMessage}
        isDisabled={!user}
      />
      <ModeOptionToggle
        inAutomaticMode={inAutomaticMode}
        setInAutomaticMode={setInAutomaticMode}
        setShowRefreshMessage={setShowRefreshMessage}
        isDisabled={!user || !isEnabled}
      />
      {showRefreshMessage && (
        <p className="color primary" style={{ margin: '1em', textAlign: 'center' }}>
          <strong>Please refresh any open Google Meet and Slides tabs after making changes.</strong>
        </p>
      )}
      <CopyUrlButton
        putExtensionUrlInClipboard={putExtensionUrlInClipboard}
        wasCopied={wasCopied}
      />
      <hr style={{ width: '100%', height: '5px' }} />
      <PleaseHelp />
      <hr style={{ width: '100%', height: '5px' }} />
      <WhatsNew />
      <hr style={{ width: '100%', height: '5px' }} />
      <Instructions />
      <hr style={{ width: '100%', height: '5px' }} />
      <Troubleshooting />
      <hr style={{ width: '100%', height: '5px' }} />
      <SayThanks />
    </div>
  );
};

interface CopyUrlButtonProps {
  putExtensionUrlInClipboard?: MouseEventHandler;
  wasCopied?: boolean;
}
const CopyUrlButton = ({
  putExtensionUrlInClipboard,
  wasCopied,
}: CopyUrlButtonProps): JSX.Element => (
  <div className="ssc_container" style={{ marginTop: '20px' }}>
    <button
      type="button"
      className="ssc_button small"
      onClick={putExtensionUrlInClipboard}
      style={{ width: '270px' }}
    >
      <img
        src={browser.runtime.getURL('assets/icons/copy-white.svg')}
        height="24"
        width="24"
        alt="Copy Extension URL"
        title="Copy Extension URL"
      />
      <p>{wasCopied ? 'URL Copied!' : 'Copy Extension URL'}</p>
    </button>
  </div>
);

const PleaseHelp = () => {
  const uninstallSelf = async () => {
    await browser.management.uninstallSelf({
      showConfirmDialog: true,
    });
  };

  return (
    <Fragment>
      <h3 id="solong" className="uppercase">
        So long and thanks for all the fish
      </h3>
      <div className="ssc_fullWidthText">
        <p>
          Well after almost 3 years, Google finally listened to our feedback and added the
          capability for presenters to share presentation control natively inside Google Meet. You
          can now add “co-presenters” when you are sharing a Google Slides presentation. For more
          information, see their{' '}
          <a
            href="https://workspaceupdates.googleblog.com/2023/04/co-present-google-slides-google-meet.html"
            target="_blank"
            rel="noreferrer noopener"
          >
            blog post
          </a>
          .
        </p>
        <p>
          <strong>
            Therefore, you no longer need this extension! Click below to uninstall it.
          </strong>
        </p>
        <div className="ssc_container" style={{ justifyContent: 'center' }}>
          <a className="ssc_button small" onClick={uninstallSelf}>
            Uninstall
          </a>
        </div>
        <p>
          I hope this extension has been useful to you over the past few years. Thank you for your
          feedback and support!
        </p>
        <p>
          Please check out my other projects at{' '}
          <a href="https://fonner.dev" target="_blank" rel="noreferrer noopener">
            fonner.dev
          </a>
          .
        </p>
      </div>
    </Fragment>
  );
};

const WhatsNew = () => (
  <Fragment>
    <h3 id="whatsnew" className="uppercase">
      WHAT&apos;S NEW
    </h3>
    <div className="ssc_fullWidthText">
      <p>
        <b>May 201, 2023 (Version 1.0.0):</b> End of the road folks. This release adds notices that
        this extension should be uninstalled now that{' '}
        <a href="#solong">Google Meet provides native slide control sharing.</a>
      </p>
      <p>
        <b>May 20, 2023 (Version 0.9.1 &amp; 0.9.2):</b> Adds link to privacy policy as per Google
        guidelines and message about Google&apos;s new &quot;co-presenters&quot; feature coming
        soon.
      </p>
      <p>
        <b>Feb 4, 2023 (Version 0.9.0):</b> New icons for Next and Previous Slide buttons:{' '}
        <img
          id="ssc_previousSlideImg"
          src={browser.runtime.getURL('assets/icons/slide-prev.svg')}
          alt="Previous Slide"
          title="Go to Previous Slide"
        />
        <img
          id="ssc_nextSlideImg"
          src={browser.runtime.getURL('assets/icons/slide-next.svg')}
          alt="Next Slide"
          title="Go to Next Slide"
        />
      </p>
      <p>
        <b>Jan 20, 2023 (Version 0.8.9):</b> Fixes issue with query strings in Meet URL preventing
        Manual Mode from working
      </p>
      <p>
        <b>Jan 15, 2023 (Version 0.8.8):</b> Adds link to the new{' '}
        <a
          href="https://fonner.gitlab.io/shared-slides-clicker/webclicker/"
          target="_blank"
          rel="noreferrer"
        >
          Web Clicker
        </a>
      </p>
      <p>
        <b>Nov 20, 2022 (Version 0.8.7):</b> Tidies up some debug logging code
      </p>
      <p>
        <b>Oct 22, 2022 (Version 0.8.6):</b> Fixes double-advance of slides in certain situations
      </p>
      <p>
        <b>Oct 3, 2022 (Version 0.8.5):</b> Adds more logging of authentication problems.
      </p>
      <p>
        <b>Sep 24-25, 2022 (Versions 0.8.2-0.8.4):</b> Adds more robust handling of authentication
        problems.
      </p>
      <p>
        <b>Sep 21, 2022 (Version 0.8.1):</b> Improves{' '}
        <a href="#troubleshooting">troubleshooting documentation</a> and error logging.
      </p>
      <p>
        <b>Sep 19, 2022 (Version 0.8.0):</b>{' '}
      </p>
      <ul>
        <li>
          Switches from Chrome login to Google login. This allows you to sign in with a different
          Google Account than your Chrome identity and should resolve issues with some users not
          being able to sign in (including users on Edge or Opera). If you encounter problems
          signing in, please let me know by{' '}
          <a href="mailto:shared-slides-clicker@googlegroups.com">emailing me</a>.
        </li>
        <li>The popup window also now shows a context-aware message based on the active tab.</li>
        <li>
          The toolbar icon badge changes to indicate active sharing or if the extension is disabled
          or not logged in.
        </li>
      </ul>
      <p>
        Thanks to Henri for his help debugging some of the issues introduced by the Manifest V3
        update!
      </p>
      <p>
        <b>Sep 17, 2022 (Version 0.7.3):</b> Better error messages. Note: Shared Slides Clicker is
        currently not supported in Edge or Opera.
      </p>
      <p>
        <b>Sep 12, 2022 (Version 0.7.2):</b> Bug fixes to further support Manifest V3. If you have
        problems, try restarting your browser.
      </p>
      <p>
        <b>Sep 10, 2022 (Version 0.7.1):</b> Bug fixes to further support Manifest V3. If you have
        problems, try restarting your browser.
      </p>
      <p>
        <b>Sep 9, 2022 (Version 0.7.0):</b> Large refactor to support{' '}
        <a href="https://developer.chrome.com/docs/extensions/mv3/intro/">
          Chrome Manifest Version 3
        </a>
        . If you encounter any problems as a result of this change, please{' '}
        <a href="mailto:shared-slides-clicker@googlegroups.com">email me</a>.
      </p>
      <p>
        <b>Jul 29, 2022 (Version 0.6.6):</b> Small fix to Instructions
      </p>
      <p>
        <b>Jun 15, 2022 (Version 0.6.5):</b> Fixes Shared Slides Clicker not appearing in Google
        Meet
      </p>
      <p>
        <b>Jan 15, 2022 (Version 0.6.4):</b> Better error handling for sign-in cancellation
      </p>
      <p>
        See older releases on&nbsp;
        <b>
          <a
            href="https://gitlab.com/fonner/shared-slides-clicker/-/releases"
            target="_blank"
            rel="noreferrer"
          >
            Gitlab
          </a>
        </b>
      </p>
    </div>
  </Fragment>
);

const Instructions = () => (
  <Fragment>
    <h3 id="instructions" className="uppercase">
      INSTRUCTIONS
    </h3>
    <ol>
      <li>
        The presenter must install the{' '}
        <a
          href="https://fonner.gitlab.io/shared-slides-clicker/"
          target="_blank"
          rel="noreferrer noopener"
        >
          Shared Slides Clicker extension
        </a>{' '}
        and sign-in using Google.
      </li>
      <li>
        <p>
          For the best experience, other slide controllers should also install the Shared Slides
          Clicker extension and use it inside Google Meet. If other slide controllers are not using
          Google Meet or cannot install the extension, they can use the&nbsp;
          <a
            href="https://fonner.gitlab.io/shared-slides-clicker/webclicker/"
            target="_blank"
            rel="noreferrer noopener"
          >
            Web Clicker
          </a>
          &nbsp;to control the presenter&apos;s slides.
        </p>
      </li>
    </ol>
    <div className="ssc_columns">
      <div className="ssc_column" style={{ width: '50%' }}>
        <span className="ssc_columnHeader">
          When automatic mode <u>is on</u>
        </span>
        <ol>
          <li value="3">
            <EnterUrlStep />
          </li>
          <li>
            <PresenterStep />
          </li>
          <li>That&apos;s it! Easy peasy!</li>
        </ol>
      </div>
      <div className="ssc_column" style={{ width: '50%' }}>
        <span className="ssc_columnHeader">
          When automatic mode <u>is off</u>
        </span>
        <ol>
          <li value="3">
            <p>
              Pin the extension to your Chrome toolbar so you can easily access it while not in
              automatic mode.
            </p>
            <p>Start by clicking the Extensions icon in the top right of your Chrome window.</p>
            <img
              src={browser.runtime.getURL('assets/images/click-extensions-icon.png')}
              width="330"
              alt="Click Extensions Icon"
            />
          </li>
          <li>
            <p>
              Click the <b>pin icon</b> next to the Shared Slides Clicker entry in your list of
              extensions
            </p>
            <img
              src={browser.runtime.getURL('assets/images/click-pin-icon.png')}
              width="330"
              alt="Click Pin Icon"
            />
          </li>
          <li>
            <p>Click the remote control icon to open Shared Slides Clicker</p>
            <img
              src={browser.runtime.getURL('assets/images/click-ssc-icon.png')}
              width="330"
              alt="Click SSC Icon"
            />
          </li>
          <li>
            <p>
              On the <b>Presentation tab</b>, the presenter will need to click the &quot;Share
              Presentation&quot; button
            </p>
            <img
              src={browser.runtime.getURL('assets/images/share-this-presentation.png')}
              width="330"
              alt="Click SSC Icon"
            />
          </li>
          <li>
            <p>
              On the <b>Meet tab</b>, the other slide controllers will need to click the &quot;Add
              Control Overlay&quot; button
            </p>
            <img
              src={browser.runtime.getURL('assets/images/add-control-overlay.png')}
              width="330"
              alt="Click SSC Icon"
            />
          </li>
          <li>
            <EnterUrlStep />
          </li>
          <li>
            <PresenterStep />
          </li>
        </ol>
      </div>
    </div>
  </Fragment>
);

const EnterUrlStep = () => (
  <div>
    <p>
      In <b>Meet</b>, other slide controllers should click the Remote button
    </p>
    <img
      src={browser.runtime.getURL('assets/images/expand-presentation-controls.png')}
      width="330"
      alt="Enter slides URL"
    />
    <p>
      Then enter the URL to the Google Slides presentation being presented and click <b>Connect</b>
    </p>
    <img
      src={browser.runtime.getURL('assets/images/meet-slides-url.png')}
      width="330"
      alt="Enter slides URL"
    />
  </div>
);

const PresenterStep = () => (
  <Fragment>
    <div>
      <p style={{ marginBottom: '0' }}>
        Once the presenter starts presenting their Google Slides, other slide controllers can{' '}
        <b>use the remote buttons</b> in the toolbar to:
      </p>
    </div>
    <ul>
      <li>Go to the next/previous slide</li>
      <li>Play/pause embedded videos</li>
      <li>Jump forward/backward 10 seconds in videos</li>
    </ul>
    <img
      className="ssc_adjacentImage"
      src={browser.runtime.getURL('assets/images/meet-control-buttons.png')}
      width="330"
      alt="Meet control buttons"
    />
  </Fragment>
);

const Troubleshooting = () => (
  <Fragment>
    <h3 id="troubleshooting" className="uppercase" style={{ marginBottom: '20px' }}>
      TROUBLESHOOTING
    </h3>
    <div>
      <label>Meet overlay controls are not advancing slides despite being connected</label>
      <ul>
        <li>
          Scroll up to the top of this page and click Sign Out and then click Sign In. Then reload
          your Meet window and try again.
        </li>
      </ul>
    </div>
    <div>
      <label>Meet overlay says &quot;Waiting for presentation&quot; </label>
      <ul>
        <li>
          Check if the presenter has started the presentation. If not, wait for them to start.
        </li>
        <li>
          Ask the presenter to click the Shared Slides Clicker icon in their Chrome extensions bar
          and then click the &quot;Share this presentation&quot; button (if present).
        </li>
        <li>
          If all else fails, ask the presenter to close their browser, re-open it and re-present.
        </li>
        <li>
          <b>NOTE</b>: for security reasons, you will not be able to connect if your Google account
          is not on the same domain (e.g., acme.com) as the person presenting.
        </li>
      </ul>
    </div>
    <div>
      <label>Keyboard shortcuts are not working </label>
      <ul>
        <li>
          When entering the URL of the Google Presentation to control, make sure to click the toggle
          next to &quot;Keyboard Shortcuts&quot;
        </li>
        <li>
          Keyboard shortcuts are temporarily disabled when the sidebar in Meet is open to prevent
          conflicts. Close the sidebar and see if keyboard shortcuts work again.
        </li>
      </ul>
    </div>
    <div>
      <label>If you still have problems</label>
      <ul>
        <li>
          Try signing out and signing back in. If that doesn&apos;t work, close all your browser
          windows and see if that helps resolve the issue.
        </li>
        <li>
          If all else fails, send an email to{' '}
          <a href="mailto:shared-slides-clicker@googlegroups.com">
            shared-slides-clicker@googlegroups.com
          </a>{' '}
          explaining the problem (include screenshots if possible).
        </li>
      </ul>
    </div>
  </Fragment>
);

const SayThanks = () => (
  <div className="ssc_container" style={{ marginTop: '20px' }}>
    <a
      className="ssc_button small"
      target="_blank"
      rel="noreferrer noopener"
      href="https://twitter.com/intent/tweet?screen_name=DevFonner&text=Thanks%20for%20the%20great%20browser%20extension%21"
      style={{ width: '270px' }}
    >
      <img
        src={browser.runtime.getURL('assets/icons/twitter-32.png')}
        alt="Say Thanks"
        title="Say Thanks"
        width="24"
        height="24"
      />
      <p>Say Thanks</p>
    </a>
  </div>
);
