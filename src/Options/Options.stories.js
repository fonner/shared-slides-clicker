/* eslint-disable */
import React from 'react';

import { Options } from './Options.tsx';

export default {
  title: 'Components/Options',
  component: Options,
  argTypes: {},
  decorators: [
    (Story) => (
      <div id="options-root">
        <Story />
      </div>
    ),
  ],
};

const Template = (args) => <Options {...args} />;

export const Main = Template.bind({});
Main.args = {};
