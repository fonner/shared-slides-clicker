/* eslint-disable */
// src/__mocks__/webextension-polyfill
// Update this file to include any mocks for the `webextension-polyfill` package
// This is used to mock these values for Jest so you can test your components
// outside the Web Extension environment provided by a compatible browser
let browserStorage = {};
const browser = {
  storage: {
    local: {
      get: jest.fn(() => browserStorage),
      set: jest.fn((object) => {
        return Promise.resolve((browserStorage = { ...browserStorage, ...object }));
      }),
      remove: jest.fn((key) => {
        return Promise.resolve(delete browserStorage[key]);
      }),
    },
  },
  runtime: {
    sendMessage(message) {
      console.log('Mocked send message', message);
      if (!message.type) {
        return Promise.resolve('Message Received');
      }
      switch (message.type) {
        case 'GOOGLE_SIGN_IN':
          return Promise.resolve({
            type: 'GOOGLE_SIGN_IN',
            success: true,
            response: { uid: 'test', displayName: 'Test User' },
          });
        case 'READY_CHECK':
          return Promise.resolve({
            type: 'READY_CHECK',
            success: true,
            response: { isEnabled: true, user: { uid: 'test', displayName: 'Test User' } },
          });
        case 'CHECK_PRESENTATION':
          return Promise.resolve({
            type: 'CHECK_PRESENTATION',
            success: true,
            response: {
              isPresentationActive: !!Math.round(Math.random()),
            },
          });
        case 'CONNECT_TO_PRESENTATION':
          return Promise.resolve({
            type: 'CONNECT_TO_PRESENTATION',
            success: !!Math.round(Math.random()),
          });
        default:
          return Promise.resolve('Message Received');
      }
    },
    getURL(url) {
      console.log('Mocked getURL', url);
      return url;
    },
    onInstalled: {
      addListener(listener) {},
    },
    onStartup: {
      addListener(listener) {},
    },
    onMessage: {
      addListener(listener) {},
    },
    setUninstallURL: jest.fn((url) => Promise.resolve()),
  },
  action: {
    setIcon: (path) => {},
    setTitle: (details) => {},
    setBadgeBackgroundColor: (color) => {},
    setBadgeText: (details) => {},
  },
  tabs: {
    query: jest.fn((query) => Promise.resolve([{ id: 1, url: query?.url }])),
    create: jest.fn((options) => Promise.resolve({ id: 1, ...options })),
    sendMessage: jest.fn((tabId, message) => Promise.resolve(`I am a response from ${tabId}`)),
  },
  notifications: {
    create: jest.fn((notificationId, options) => Promise.resolve('done')),
  },
};

const resetStorage = () => {
  browserStorage = {};
};
export { browser, resetStorage };
