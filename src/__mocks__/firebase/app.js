/* eslint-disable */
// src/__mocks__/firebase/app
// Update this file to include any mocks for the `firebase/app` package
// This is used to mock these values for Jest so you can test your components
// outside the Web Extension environment provided by a compatible browser

export function setLogLevel(logLevel) {
  // console.log('mock setLogLevel called with', logLevel);
}

export function initializeApp(config) {
  // console.log('mock initializeApp', config);
}
