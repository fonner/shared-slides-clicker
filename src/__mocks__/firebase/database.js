/* eslint-disable */

/* Test helpers */
const db = new Map();
const serverDatetime = new Date();
export const getServerDatetime = () => serverDatetime;
export const resetDatabase = () => db.clear();

/* Mocked functions */
export function getDatabase() {}
export function ref(database, location) {
  return location;
}

export async function get(location) {
  const value = db.get(location);
  return Promise.resolve({
    exists: () => !!value,
    val: () => value,
  });
}

export const set = jest
  .fn((location, value) => {
    db.set(location, {
      exists: () => true,
      val: () => value,
    });
  })
  .mockName('mockFirebaseSet');

export const onValue = jest
  .fn((location, callback) => {
    callback(db.get(location));
    return jest.fn();
  })
  .mockName('mockFirebaseOnValue');

export const onChildAdded = jest
  .fn((location, callback) => {
    db.set(location, {
      exists: () => true,
      val: () => 'some value',
    });
    callback(db.get(location));
    return jest.fn();
  })
  .mockName('mockFirebaseOnChildAdded');

export const remove = jest
  .fn((location) => {
    db.delete(location);
    db.delete(`${location}/commands`);
  })
  .mockName('mockFirebaseRemove');

export const push = jest
  .fn((location, value) => {
    const newId = serverDatetime.getMilliseconds();
    const newValue = {
      exists: () => true,
      val: () => value,
    };
    db.set(`${location}/${newId}`, newValue);
    return newValue;
  })
  .mockName('mockFirebasePush');

export function query(location, orderBy, start) {
  return location;
}
export function child(location, childLocation) {
  return `${location}/${childLocation}`;
}
export function orderByChild() {}
export function startAt() {}

export function serverTimestamp() {
  return serverDatetime;
}
