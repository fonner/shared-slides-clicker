/* eslint-disable */

/* Test helpers */
const defaultUser = { uid: 'testUser', email: 'testuser@gmail.com', displayName: 'Testee Tester' };
let currUser = defaultUser;

export function setTestUser(user) {
  currUser = user;
}
export function resetTestUser() {
  currUser = defaultUser;
}
export function getTestUser() {
  return currUser;
}

/* Mocked functions */
export function getAuth() {
  return {};
}
export function onAuthStateChanged(auth, nextObserver, error) {
  setTimeout(() => nextObserver(currUser), 100);
  return jest.fn();
}
export const signInWithPopup = jest.fn((auth, provider) => Promise.resolve({ user: currUser }));
export const signOut = jest.fn((auth) => Promise.resolve());
export function setPersistence() {}
export function browserLocalPersistence() {}
export function GoogleAuthProvider() {
  return {
    addScope: (scope) => {
      console.log('GoogleAuthProvider.addScope', scope);
    },
  };
}
