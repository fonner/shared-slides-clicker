/* eslint-disable security/detect-object-injection */
import { browser } from 'webextension-polyfill-ts';
import * as Sentry from '@sentry/browser';

import {
  JustSuccessResponseMessage,
  ManualModeStatusResponseMessage,
  ToggleManualModeResponseMessage,
  MeetControlRequestMessage,
  PresentationControlRequestMessage,
} from '../@types/messages.d';
import MESSAGE_TYPE from '../utils/messageConstants';

/**
 * Gets whether SSC is in manual mode (true = yes, false = no)
 * Used by backgroundPage isReadyCheck
 * @returns Promise<ManualModeStatusResponseMessage>
 */
export async function getIsInAutomaticMode(): Promise<boolean> {
  const localStorage = await browser.storage.local.get();
  const isManual = localStorage && localStorage.isManualMode;
  return !isManual;
}

/**
 * Toggles manual mode on and off, called from Options page
 * @returns Promise<ManualModeStatusResponseMessage>
 */
export async function toggleMode(): Promise<ToggleManualModeResponseMessage> {
  const isInAutomaticMode = await getIsInAutomaticMode();

  // Toggle by flipping the value from isEnabled to isDisabled
  const newIsInAutomaticMode = !isInAutomaticMode;
  // Store is off, not is on, so when it's null to start, we treat that as being on
  return browser.storage.local.set({ isManualMode: !newIsInAutomaticMode }).then(() => {
    console.log('Toggle Mode Success! In Automatic Mode:', newIsInAutomaticMode);
    const response: ToggleManualModeResponseMessage = {
      type: MESSAGE_TYPE.TOGGLE_MODE,
      success: true,
      response: { isInAutomaticMode: newIsInAutomaticMode },
    };
    return response;
  });
}

/**
 * Used by other Popup to keep track in browser storage of whether a manual-mode
 * action was taken for a given url. This is called when the user is manually [de]activates SSC.
 * @param sourceUrl
 * @returns Promise<JustSuccessResponseMessage>
 */
async function addManualButtonClick(sourceUrl: string): Promise<void> {
  console.log(`addManualButtonClick: ${sourceUrl}`);
  const url = new URL(sourceUrl);
  const { pathname } = url;

  const newStorageItem: {
    [key: string]: boolean;
  } = {};
  const key = `manual-click-${pathname}`;
  newStorageItem[key] = true;
  return browser.storage.local.set(newStorageItem);
}

/**
 * Used by Popup to remove from browser storage the record of a manual-mode
 * action taken for a given url.  This is called when the user is manually [de]activates SSC.
 * @param sourceUrl
 * @returns Promise<JustSuccessResponseMessage>
 */
async function removeManualButtonClick(sourceUrl: string): Promise<void> {
  console.log(`removeManualButtonClick:${sourceUrl}`);
  const url = new URL(sourceUrl);
  const { pathname } = url;
  const key = `manual-click-${pathname}`;
  return browser.storage.local.remove(key);
}

/**
 * Called via Popup to track whether current url has been manually shared
 * @param sourceUrl
 * @returns Promise<ManualModeStatusResponseMessage>
 */
export async function getManualButtonStatus(
  sourceUrl: string
): Promise<ManualModeStatusResponseMessage> {
  // console.log('wasManualButtonClicked', sourceUrl);

  const url = new URL(sourceUrl);
  const { pathname } = url;

  const key = `manual-click-${pathname}`;
  const localStorage = await browser.storage.local.get();
  const manualClickValue = localStorage[key];
  const type = sourceUrl.startsWith('https://meet.google.com')
    ? MESSAGE_TYPE.MANUAL_CONTROL_STATUS
    : MESSAGE_TYPE.MANUAL_PRESENTATION_STATUS;
  const response: ManualModeStatusResponseMessage = {
    type,
    success: true,
    response: { wasClicked: !!manualClickValue },
  };
  return response;
}

/**
 * Called by these methods to tell presentation content script to start/stop sharing
 * @param presentationUrl
 * @param startSharing
 * @returns
 */
export async function sendManualControlMessageToPresentationTab(
  presentationUrl: string,
  startSharing: boolean
): Promise<void> {
  console.log('sendManualControlMessageToPresentationTab', presentationUrl);
  const type = startSharing ? MESSAGE_TYPE.SHARE_PRESENTATION : MESSAGE_TYPE.UNSHARE_PRESENTATION;
  const presentationId = getPresentationIdFromUrl(presentationUrl) || '';
  const message: PresentationControlRequestMessage = {
    type,
    presentationId,
    startSharing,
  };
  return sendMessageToTabByUrl(presentationUrl, message);
}

/**
 * Called these methods to tell meet content script to show/hide SSC button
 * @param meetUrl
 * @param showButton
 * @returns
 */
function sendManualControlMessageToMeetTab(meetUrl: string, showButton: boolean): void {
  const message: MeetControlRequestMessage = {
    type: MESSAGE_TYPE.SHARE_CONTROL,
    meetUrl,
    startControl: showButton,
  };
  sendMessageToTabByUrl(meetUrl, message);
}

/**
 * Called when presentationContentScript is unloading to signal the end of sharing.
 * @param presentationUrl
 * @returns
 */
export async function cleanupPresentationManualModeData(
  presentationUrl: string
): Promise<JustSuccessResponseMessage> {
  console.log(`unsharePresentation for ${presentationUrl}`);

  const presentationId = getPresentationIdFromUrl(presentationUrl);
  if (!presentationId) {
    return Promise.resolve({ type: MESSAGE_TYPE.UNSHARE_PRESENTATION, success: false });
  }
  removeManualButtonClick(presentationUrl);

  const response: JustSuccessResponseMessage = {
    type: MESSAGE_TYPE.UNSHARE_PRESENTATION,
    success: true,
  };
  return response;
}

/**
 * Called by Popup to handle a manual button click on a Presentation tab
 * @param presentationUrl
 * @param startSharing
 * @returns
 */
export function handlePresentationManualButtonClick(
  presentationUrl: string,
  startSharing: boolean
): void {
  if (startSharing) {
    addManualButtonClick(presentationUrl);
    sendManualControlMessageToPresentationTab(presentationUrl, true);
  } else {
    removeManualButtonClick(presentationUrl);
    sendManualControlMessageToPresentationTab(presentationUrl, false);
  }
}

/**
 * Called by Popup to handle a manual button click on a Meet tab
 * @param meetUrl
 * @param startSharing
 * @returns
 */
export function handleMeetManualButtonClick(meetUrl: string, startSharing: boolean): void {
  if (startSharing) {
    addManualButtonClick(meetUrl);
    sendManualControlMessageToMeetTab(meetUrl, true);
  } else {
    removeManualButtonClick(meetUrl);
    sendManualControlMessageToMeetTab(meetUrl, false);
  }
}

/*
  Receiving tab needs to call browser.runtime.onMessage.addListener
  which takes (request, sender, sendResponse) parameters
*/
export async function sendMessageToTabById(tabId: number, message: unknown): Promise<void> {
  console.log(`Sending message to tab by id ${tabId}:`, message);
  return browser.tabs
    .sendMessage(tabId, message)
    .then((response) => {
      if (!response) {
        console.log('No response from sending message to tab', message);
      } else {
        console.log('Response from sending message to tab:', response);
      }
    })
    .catch((error) => {
      // If the tab is not found, it's probably because the tab has been closed
      if (error?.message?.includes('Receiving end does not exist')) {
        return;
      }
      Sentry.captureException(error, {
        extra: { errorMessage: `Unable to send message to tab by id`, message },
      });
      console.error('Unable to send message to tab:', error);
    });
}

export async function sendMessageToTabByUrl(url: string, message: unknown): Promise<void> {
  console.log(`Sending message to tab by url ${url}:`, message);
  return browser.tabs
    .query({ url: `${url}*` })
    .then((tabs) => {
      // eslint-disable-next-line prefer-const
      for (let tab of tabs) {
        console.log(`Found tab id ${tab.id} for url ${url}, sending:`, message);
        if (tab.id) sendMessageToTabById(tab.id, message);
      }
    })
    .catch((error) => {
      Sentry.captureException(error, {
        extra: { errorMessage: `Unable to send message to tab by url`, message },
      });
      console.error('Error sending message to active tab:', error);
    });
}

export function getPresentationIdFromUrl(presentationUrl: string): string | null {
  const pathPieces = presentationUrl.split('/');
  if (pathPieces.length < 6) {
    console.error('Unable to find presentationId');
    return null;
  }
  let presentationId = pathPieces[5];
  if (presentationId.indexOf('?') >= 0) {
    presentationId = presentationId.slice(0, presentationId.indexOf('?'));
  }
  return presentationId;
}
