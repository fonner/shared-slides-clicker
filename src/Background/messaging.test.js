import chrome from 'sinon-chrome';
import { browser } from 'webextension-polyfill-ts';

import { getTestUser, signOut } from '../__mocks__/firebase/auth';
import { resetDatabase } from '../__mocks__/firebase/database';

import { handleMessage } from './background';
import MESSAGE_TYPE from '../utils/messageConstants';

// const testId = '14pIFAJVUX83RF0-F0Oh7AbL1rDLBdY2vHdOHAfBBi1M';
// const testUrl = `https://docs.google.com/presentation/d/${testId}/edit#slide=id.ga0c5ccff23_0_31`;
const testUser = getTestUser();

beforeAll(() => {
  chrome.identity.clearAllCachedAuthTokens = () => {
    console.log('Clearing all cached auth tokens');
  };
  global.chrome = chrome;
});

describe('Messages', () => {
  beforeEach(() => {
    resetDatabase();
  });

  // test('googleSignIn', async () => {
  //   const { type, success, response } = await handleMessage({
  //     type: MESSAGE_TYPE.GOOGLE_SIGN_IN,
  //   });
  //   expect(type).toBe(MESSAGE_TYPE.GOOGLE_SIGN_IN);
  //   expect(success).toBe(true);
  //   expect(response).toBe(testUser);
  //   expect(signInWithPopup).toHaveBeenCalled();
  // });

  test('googleSignOut', async () => {
    const { type, success } = await handleMessage({
      type: MESSAGE_TYPE.GOOGLE_SIGN_OUT,
    });
    expect(type).toBe(MESSAGE_TYPE.GOOGLE_SIGN_OUT);
    expect(success).toBe(true);
    expect(signOut).toHaveBeenCalled();
  });

  test('openOptions', async () => {
    await handleMessage({
      type: MESSAGE_TYPE.OPEN_OPTIONS,
      section: 'troubleshooting',
    });
    expect(browser.tabs.create).toHaveBeenCalled();
  });

  test('toggleEnabled', async () => {
    let responseMessage = await handleMessage({
      type: MESSAGE_TYPE.TOGGLE_ENABLED,
    });
    expect(responseMessage.type).toBe(MESSAGE_TYPE.TOGGLE_ENABLED);
    expect(responseMessage.success).toBe(true);
    expect(responseMessage.response.isEnabled).toBe(false);

    responseMessage = await handleMessage({
      type: MESSAGE_TYPE.TOGGLE_ENABLED,
    });
    expect(responseMessage.response.isEnabled).toBe(true);
  });

  test('toggleMode', async () => {
    let responseMessage = await handleMessage({
      type: MESSAGE_TYPE.TOGGLE_MODE,
    });
    expect(responseMessage.type).toBe(MESSAGE_TYPE.TOGGLE_MODE);
    expect(responseMessage.success).toBe(true);
    expect(responseMessage.response.isInAutomaticMode).toBe(false);

    responseMessage = await handleMessage({
      type: MESSAGE_TYPE.TOGGLE_MODE,
    });
    expect(responseMessage.response.isInAutomaticMode).toBe(true);
  });

  test('isReadyCheck', async () => {
    const responseMessage = await handleMessage({
      type: MESSAGE_TYPE.READY_CHECK,
    });
    const { type, success, response } = responseMessage;
    expect(type).toBe(MESSAGE_TYPE.READY_CHECK);
    expect(success).toBe(true);
    const { user, isEnabled, inAutomaticMode } = response;
    expect(user).toBe(testUser);
    expect(isEnabled).toBe(true);
    expect(inAutomaticMode).toBe(true);
  });

  test('isReadyCheck not ready', async () => {
    await handleMessage({
      type: MESSAGE_TYPE.TOGGLE_ENABLED,
    });

    const responseMessage = await handleMessage({
      type: MESSAGE_TYPE.READY_CHECK,
    });
    const { type, success, response } = responseMessage;
    expect(type).toBe(MESSAGE_TYPE.READY_CHECK);
    expect(success).toBe(true);
    const { user, isEnabled } = response;
    expect(user).toBe(testUser);
    expect(isEnabled).toBe(false);
  });

  // TODO: move to MeetContentScript.test.js
  // test('checkPresentationStatus valid', async () => {
  //   const domain = getEmailDomain(testUser);
  //   await set(`${domain}/${testId}`, {
  //     info: {
  //       initiatedBy: testUser.uid,
  //     },
  //   });
  //   const responseMessage = await handleMessage({
  //     type: MESSAGE_TYPE.CHECK_PRESENTATION,
  //     presentationUrl: testUrl,
  //   });
  //   expect(responseMessage.type).toBe(MESSAGE_TYPE.CHECK_PRESENTATION);
  //   expect(responseMessage.success).toBe(true);
  //   expect(responseMessage.response.isPresentationActive).toBe(true);
  // });

  // test('checkPresentationStatus invalid', async () => {
  //   const responseMessage = await handleMessage({
  //     type: MESSAGE_TYPE.CHECK_PRESENTATION,
  //     presentationUrl: testUrl,
  //   });
  //   expect(responseMessage.type).toBe(MESSAGE_TYPE.CHECK_PRESENTATION);
  //   expect(responseMessage.success).toBe(true);
  //   expect(responseMessage.response.isPresentationActive).toBe(false);
  // });

  // test('stopPresentation', async () => {
  //   const domain = getEmailDomain(testUser);
  //   await set(`${domain}/${testId}`, {
  //     info: {
  //       initiatedBy: testUser.uid,
  //     },
  //   });

  //   const responseMessage = await handleMessage(
  //     {
  //       type: MESSAGE_TYPE.STOP_PRESENTATION,
  //       presentationId: testId,
  //     },
  //     { tab: { id: 1 } }
  //   );
  //   expect(responseMessage.type).toBe(MESSAGE_TYPE.STOP_PRESENTATION);
  //   expect(responseMessage.success).toBe(true);

  //   const snapshot = await get(`${domain}/${testId}`);
  //   expect(snapshot.exists()).toBe(false);
  // });

  // test('connectToPresentation', async () => {
  //   const domain = getEmailDomain(testUser);

  //   const responseMessage = await handleMessage({
  //     type: MESSAGE_TYPE.CONNECT_TO_PRESENTATION,
  //     presentationUrl: testUrl,
  //   });
  //   expect(responseMessage.type).toBe(MESSAGE_TYPE.CONNECT_TO_PRESENTATION);
  //   expect(responseMessage.success).toBe(true);
  //   const connectedUserKey = `${domain}/${testId}/users/${testUser.uid}`;
  //   const snapshot = await get(connectedUserKey);
  //   expect(snapshot.exists()).toBe(true);
  // });

  test('unshareControl', async () => {
    // covered by backgroundUtils.test.js
  });
  test('unsharePresentation', async () => {
    // covered by backgroundUtils.test.js
  });
  // end of messaging tests
});
