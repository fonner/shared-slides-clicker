/* eslint-disable import/no-extraneous-dependencies */
import { browser, Runtime } from 'webextension-polyfill-ts';
import * as Sentry from '@sentry/browser';
import { getIsEnabled } from './backgroundUtils';

import {
  getIsInAutomaticMode,
  toggleMode,
  // wasManualButtonClicked,
  // sharePresentation,
  // shareControl,
} from './manualModeUtils';

import { getCurrentUser, googleSignIn, googleSignOut } from './firebase';

import MESSAGE_TYPE from '../utils/messageConstants';

// import { READY_ICONS, NOT_READY_ICONS, PRESENTING_ICONS } from '../utils/iconConstants';
import {
  ReadyCheckResponseMessage,
  ToggleEnabledResponseMessage,
  JustSuccessResponseMessage,
  AllRequestMessages,
  AllResponseMessages,
  AuthChangedResponseMessage,
  BrowserNotifyResponseMessage,
} from '../@types/messages';

function openOptionsPage(section?: string): void {
  const optionsUrl = section ? `options.html#${section}` : 'options.html';
  browser.tabs.create({ url: optionsUrl }).catch((error) => {
    if (error?.message?.includes('No current window')) {
      return;
    }
    Sentry.captureException(error, {
      extra: {
        errorMessage: `Error opening options page to section ${section}`,
      },
    });
    console.log(`Error opening options page to section ${section}`);
  });
}

function initSentry(): void {
  console.log('✨ ~ Initializing Sentry ~ ✨');
  Sentry.init({
    dsn: 'https://565146604b21492e9998f275e423d658@o358776.ingest.sentry.io/6074661',
    release: `${process.env.APP_VERSION}`,
  });
}
initSentry();

browser.runtime.onInstalled.addListener(async (details) => {
  const isUpdating = details && details.reason === 'update';
  const useEmulator = process.env.USE_EMULATOR === 'true';
  console.log(
    `🚀 ~ Extension ${isUpdating ? 'updated' : 'installed'} ${
      useEmulator ? 'and using emulator ' : ''
    }~ 🚀`
  );
  await googleSignIn();
  updateIcon();

  const isReadyResponse = await isReadyCheck();
  if (!isReadyResponse.success || !isReadyResponse.response) {
    openOptionsPage();
  } else {
    const { user, isEnabled } = isReadyResponse.response;
    if (!user || !isEnabled) {
      openOptionsPage();
    }
  }
  if (isUpdating) {
    sendBrowserNotification(
      'Shared Slides Clicker has been updated!',
      "Click the icon and click the gear to see what's new!"
    );
  }
});

browser.runtime.onStartup.addListener(async () => {
  const useEmulator = process.env.USE_EMULATOR === 'true';

  console.log(`🚀 ~ Extension Started ${useEmulator ? 'and using emulator ' : ''}~ 🚀`);
  await googleSignIn();
  updateIcon();
});

// browser.runtime.setUninstallURL('https://fonner.gitlab.io/shared-slides-clicker/uninstall/').then(
//   () => {
//     /* ignore success */
//   },
//   (error) => {
//     console.error(`Error setting uninstall URL: ${error}`);
//     Sentry.captureException(error, {
//       extra: { errorMessage: `Unable to set uninstall URL` },
//     });
//   }
// );

async function updateIcon(isPresenting?: boolean): Promise<void> {
  /* Until this bug is fixed, setIcon throws an error
     See https://bugs.chromium.org/p/chromium/issues/detail?id=1262029&q=setIcon&can=2
     Still broken as of Sept 10, 2022
  */
  // TODO: uncomment setIcon calls when above Chrome bug is fixed
  if (isPresenting) {
    console.log('Updating Icon to Presenting');
    browser.action.setTitle({ title: 'Shared Slides Clicker is sharing your slides!' });
    browser.action.setBadgeBackgroundColor({ color: '#1FFF2A' });
    browser.action.setBadgeText({ text: '✔' });
    // chrome.action?.setIcon({
    //   path: PRESENTING_ICONS,
    // });
  } else {
    const isSignedIn = await getCurrentUser();
    const isEnabled = await getIsEnabled();
    if (isSignedIn && isEnabled) {
      console.log('Updating Icon to Ready (signed in and enabled)');
      browser.action.setTitle({ title: 'Shared Slides Clicker is ready!' });
      browser.action.setBadgeText({ text: '' });
      // chrome.action?.setIcon({
      //   path: READY_ICONS,
      // });
    } else {
      console.log('Updating icon to Not Ready (not signed in and/or not enabled)');
      browser.action.setTitle({
        title: 'Shared Slides Clicker is not signed in and/or not enabled!',
      });
      browser.action.setBadgeBackgroundColor({ color: '#F60000' });
      browser.action.setBadgeText({ text: '✖' });
      // chrome.action?.setIcon({
      //   path: NOT_READY_ICONS,
      // });
    }
  }
}

export async function isReadyCheck(tabId?: number | null): Promise<ReadyCheckResponseMessage> {
  console.log(`Doing isReadyCheck for tab ${tabId}`);

  // Custom logic to handle initial load where getAuth().currentUser hasn't been set yet
  const currUser = await getCurrentUser();
  const isEnabled = await getIsEnabled();
  const inAutomaticMode = await getIsInAutomaticMode();
  console.log(
    `Current user: ${
      currUser ? currUser.uid : 'not signed in'
    }, isReadyCheck isEnabled: ${isEnabled}, and inAutomaticMode: ${inAutomaticMode}`
  );

  updateIcon();
  const message: ReadyCheckResponseMessage = {
    type: MESSAGE_TYPE.READY_CHECK,
    success: true,
    response: {
      user: currUser,
      tabId: tabId || null,
      isEnabled,
      inAutomaticMode,
    },
  };
  return Promise.resolve(message);
}

async function doGoogleSignIn(): Promise<AuthChangedResponseMessage> {
  console.log('Doing Google Sign In');
  const message = await googleSignIn();
  updateIcon();
  return message;
}

async function doGoogleSignOut(): Promise<JustSuccessResponseMessage> {
  console.log('Doing Google Sign Out');
  const message = await googleSignOut();
  updateIcon();
  return message;
}

async function toggleEnabled(): Promise<ToggleEnabledResponseMessage> {
  const isEnabled = await getIsEnabled();

  // Toggle by flipping the value from isEnabled to isDisabled
  const newIsEnabled = !isEnabled;
  // Store isDisabled, not isEnabled, so when it's null to start, we treat that as being enabled
  return browser.storage.local.set({ isDisabled: !newIsEnabled }).then(() => {
    console.log(`Toggled Is Enabled to: ${newIsEnabled}`);
    updateIcon();
    return {
      type: MESSAGE_TYPE.TOGGLE_ENABLED,
      success: true,
      response: { isEnabled: newIsEnabled },
    };
  });
}

export async function sendBrowserNotification(
  title: string | null,
  message: string | null
): Promise<BrowserNotifyResponseMessage | null> {
  if (title && message) {
    try {
      const notificationId = await browser.notifications.create('ssc_action', {
        type: 'basic',
        iconUrl: browser.runtime.getURL('assets/icons/remote.png'),
        title,
        message,
      });
      // return notifId;
      return Promise.resolve({
        type: MESSAGE_TYPE.BROWSER_NOTIFY,
        success: true,
        response: { notificationId },
      });
    } catch (error) {
      Sentry.captureException(error, { extra: { message: `Unable to send browser notification` } });
      console.warn(`Unable to send browser notification ${title}`, error);
      return null;
    }
  } else {
    console.warn('Cannot send blank browser notification');
    return null;
  }
}

/* ** This is the main background script router ** */

// Using sendResponse is deprecated. Instead return a promise.
export async function handleMessage(
  message: AllRequestMessages,
  sender: Runtime.MessageSender /* , _sendResponse */
): Promise<void | AllResponseMessages | null> {
  const tabId = sender?.tab?.id || null;
  switch (message.type) {
    /* Extension plumbing, ignore */
    case 'SIGN_CONNECT':
      return null;

    /* Set up and config */
    case MESSAGE_TYPE.GOOGLE_SIGN_IN:
      return doGoogleSignIn();
    case MESSAGE_TYPE.GOOGLE_SIGN_OUT:
      return doGoogleSignOut();
    case MESSAGE_TYPE.OPEN_OPTIONS:
      return openOptionsPage(message.section);
    case MESSAGE_TYPE.TOGGLE_ENABLED:
      return toggleEnabled();
    case MESSAGE_TYPE.TOGGLE_MODE:
      return toggleMode();
    case MESSAGE_TYPE.READY_CHECK:
      return isReadyCheck(tabId);

    /* Common actions */
    case MESSAGE_TYPE.UPDATE_ICON:
      return updateIcon(message.isPresenting);
    case MESSAGE_TYPE.BROWSER_NOTIFY:
      return sendBrowserNotification(message.title, message.message);

    default:
      console.log('Ignoring unknown message ', message);
      return null;
  }
}
browser.runtime.onMessage.addListener(handleMessage);
