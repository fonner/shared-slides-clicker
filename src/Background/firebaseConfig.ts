export const firebaseConfig = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  /*
    Emulator doesn't enforce security rules, so sometimes you gotta
    test against the real database
  */
  // databaseURL: process.env.FIREBASE_DATABASE_URL,
  databaseURL:
    process.env.USE_EMULATOR === 'true'
      ? 'http://localhost:9000/?ns=slidessharedclicker'
      : process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGE_SENDER_ID,
  appId: process.env.FIREBASE_APP_ID,
  measurementId: process.env.FIREBASE_MEASUREMENT_ID,
};
