/* eslint-disable import/no-extraneous-dependencies */
import * as Sentry from '@sentry/browser';
import { browser, Manifest } from 'webextension-polyfill-ts';
import { initializeApp, setLogLevel } from 'firebase/app';
import {
  getAuth,
  onAuthStateChanged,
  signInWithCredential,
  UserCredential,
  signOut,
  GoogleAuthProvider,
  User,
} from 'firebase/auth';
import {
  getDatabase,
  ref,
  child,
  get,
  set,
  onValue,
  onChildAdded,
  query,
  orderByChild,
  startAt,
  remove,
  push,
  DataSnapshot,
  DatabaseReference,
  Unsubscribe,
} from 'firebase/database';
import { JustSuccessResponseMessage, AuthChangedResponseMessage } from '../@types/messages.d';

import { firebaseConfig } from './firebaseConfig';

import MESSAGE_TYPE from '../utils/messageConstants';
// import { getChromeIdentity } from './backgroundUtils';

console.log('🔥 ~ Firebase Initializing... ~ 🔥');

export const firebaseApp = initializeApp(firebaseConfig);
export const auth = getAuth(firebaseApp);
export const database = getDatabase(firebaseApp);

setLogLevel('error'); /* hide Firebase SDK warning log messages */

export async function getCurrentUser(): Promise<User | null> {
  const currUser: User | null = await new Promise((resolve, reject) => {
    const { currentUser } = auth;
    if (currentUser) {
      resolve(currentUser);
    } else {
      const unsubscribe = onAuthStateChanged(
        auth,
        (user) => {
          unsubscribe();
          resolve(user);
        },
        reject
      );
    }
  });
  // console.log('Firebase: current user', currUser);
  return currUser;
}

/* Warning: do not call from a Content Script because getAuthTokenViaWebAuthFlow will fail.
   Instead, only call googleSignInWithToken from Content Scripts */
export async function googleSignIn(): Promise<AuthChangedResponseMessage> {
  console.log('Firebase: Starting Google Sign-In');
  const currentUser = await getCurrentUser();
  if (currentUser) {
    console.log('Firebase: Already signed in');
    return { type: MESSAGE_TYPE.GOOGLE_SIGN_IN, success: true, response: currentUser };
  }
  try {
    /* Use Web Auth Flow instead of Chrome Identity to support Edge and Opera
       and also because Chrome Identity sometimes becomes stale, requiring a browser restart
    */
    // const token = await getChromeIdentity();
    const token = await getAuthTokenViaWebAuthFlow();
    const message = await googleSignInWithToken(token);
    return message;
  } catch (error) {
    console.warn(`Firebase: Error signing in with Google`, error);
    const msg: string = (error as Error)?.message || (error as string);
    if (
      msg !== 'The user did not approve access.' &&
      msg !== 'OAuth2 not granted or revoked.' &&
      msg !== 'Authorization page could not be loaded.'
    ) {
      Sentry.captureMessage(`googleSignIn problem: ${msg}`);
    }
    return { type: MESSAGE_TYPE.GOOGLE_SIGN_IN, success: false, response: `${msg}` };
  }
}

/* Warning: This will fail if called from a Content Script */
export async function getAuthTokenViaWebAuthFlow(): Promise<string> {
  let authURL;
  try {
    const redirectURL = browser.identity.getRedirectURL('oauth2');
    const { oauth2 } = browser.runtime.getManifest() as Manifest.ManifestBase & {
      oauth2: { client_id: string };
    };
    const clientId = oauth2.client_id;
    const authParams = new URLSearchParams({
      client_id: clientId,
      response_type: 'token',
      redirect_uri: redirectURL,
      scope: ['email', 'profile'].join(' '),
    });
    authURL = `https://accounts.google.com/o/oauth2/auth?${authParams.toString()}`;
    console.info('Firebase: Launching Web Auth Flow with auth URL', authURL);
    const responseUrl = await browser.identity.launchWebAuthFlow({
      url: authURL,
      interactive: true,
    });
    const url = new URL(responseUrl);
    const urlParams = new URLSearchParams(url.hash.slice(1));
    const {
      access_token: token,
      token_type: tokenType,
      expires_in: expiresIn,
    } = Object.fromEntries(urlParams.entries());
    console.debug(
      'Firebase: Got %s access token %s from Web Auth Flow, expires in %s',
      tokenType,
      token,
      expiresIn
    );
    await browser.storage.local.set({ token });
    return token;
  } catch (error) {
    console.warn(`Firebase: Error getting token from webAuthFlow`, error);
    const msg: string = (error as Error)?.message || (error as string);
    if (msg !== 'The user did not approve access.' && msg !== 'OAuth2 not granted or revoked.') {
      Sentry.captureException(error, {
        extra: {
          errorMessage: `Error opening authorization page ${authURL}`,
        },
      });
    }
    throw error;
  }
}

// This method is safe to call from Content Scripts but do not add a call to
// getAuthTokenViaWebAuthFlow which will break in Content Scripts
export async function googleSignInWithToken(token: string): Promise<AuthChangedResponseMessage> {
  console.log('Starting Google Sign-In with Token', token);
  try {
    // https://firebase.google.com/docs/auth/web/auth-state-persistence
    // DO NOT SET PERSISTENCE: it causes a ReferenceError: window is not defined
    // See https://github.com/firebase/firebase-js-sdk/issues/2903
    // await setPersistence(auth, browserLocalPersistence);
    const oAuthCredential = GoogleAuthProvider.credential(null, token);
    // Sign in to Firebase with the OAuth Access Token
    const credential: UserCredential = await signInWithCredential(auth, oAuthCredential);
    console.log('Firebase: Google Sign-In with Token Success', credential);
    return { type: MESSAGE_TYPE.GOOGLE_SIGN_IN, success: true, response: credential.user };
  } catch (error) {
    console.warn(`Firebase: Error signing in with token`, error);
    const msg: string = (error as Error)?.message || (error as string);
    Sentry.captureMessage(`googleSignInWithToken problem: ${msg}`);
    return { type: MESSAGE_TYPE.GOOGLE_SIGN_IN, success: false, response: `${msg}` };
  }
}

export async function googleSignOut(): Promise<JustSuccessResponseMessage> {
  console.log('Firebase: Starting Google Sign-Out');
  try {
    await signOut(auth);
    console.log('Firebase: Google Sign-Out Success');
    const token = await browser.storage.local.get('token');
    await chrome.identity.removeCachedAuthToken({ token: token.token }, () => {
      console.log('Firebase: Cleared cached auth token');
    });
    return { type: MESSAGE_TYPE.GOOGLE_SIGN_OUT, success: true };
  } catch (error) {
    console.warn('Firebase: Google Sign-Out Failure ', error);
    return { type: MESSAGE_TYPE.GOOGLE_SIGN_OUT, success: false };
  }
}

export async function getDatabaseSnapshot(location: string): Promise<DataSnapshot> {
  const reference = ref(database, location);
  const snapshot = await get(reference);
  return snapshot;
}

export async function setInDatabase(location: string, value: unknown): Promise<void> {
  const reference = ref(database, location);
  const response = await set(reference, value);
  return response;
}

export async function addToDatabase(location: string, value: unknown): Promise<DatabaseReference> {
  const reference = ref(database, location);
  const response = await push(reference, value);
  return response;
}

export async function deleteFromDatabase(location: string): Promise<void> {
  const reference = ref(database, location);
  const response = await remove(reference);
  return response;
}

/**
 * Listens for data changes at a particular location.
 *
 * @param  {} location
 * @param  {} callback - A callback that fires when the specified event occurs. The
 * callback will be passed a DataSnapshot.
 *
 * @returns A function that can be invoked to remove the listener.
 */
export async function onDatabaseValueChange(
  location: string,
  callback: (snapshot: DataSnapshot) => unknown
): Promise<Unsubscribe> {
  const reference = ref(database, location);
  return onValue(reference, callback);
}

/**
 * Listens for a new, most recent child to be added at a particular location.
 *
 * @param  {} parentLocation - the path to the parent, e.g., 'example/1234'
 * @param  {} childLocation - the relative path to the parent, .e.g., 'commands'
 * @param  {} callback - A callback that fires when the specified event occurs. The
 * callback will be passed a DataSnapshot.
 *
 * @returns A function that can be invoked to remove the listener.
 */
export async function onChildAddedToDatabase(
  parentLocation: string,
  childLocation: string,
  callback: (snapshot: DataSnapshot) => unknown
): Promise<Unsubscribe> {
  const reference = ref(database, parentLocation);
  return onChildAdded(
    query(child(reference, childLocation), orderByChild('timestamp'), startAt(Date.now())),
    callback
  );
}
