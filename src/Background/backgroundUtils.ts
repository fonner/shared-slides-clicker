import { browser } from 'webextension-polyfill-ts';
import { User } from 'firebase/auth';

export function getEmailDomain(user: User | null): string {
  if (!user) {
    throw new Error('Unable to get email domain for null user');
  }
  const { email } = user;
  const domain = email?.split('@')[1];
  return domain ? domain.replace(/\./g, '_') : '';
}

export async function getIsEnabled(): Promise<boolean> {
  const localStorage = await browser.storage.local.get();
  const isDisabled = localStorage && localStorage.isDisabled;
  return !isDisabled;
}

export function buildPresentationActionNotification(
  action: string,
  name: string
): [string, string] {
  const message = `Received Shared Slides Clicker command from ${name}`;
  let title = '';
  switch (action) {
    case 'left':
      title = 'Moving to previous slide';
      break;
    case 'right':
      title = 'Moving to next slide';
      break;
    case 'playPauseVideo':
      title = 'Play/pause video';
      break;
    case 'toggleFullScreenVideo':
      title = 'Toggling video full screen';
      break;
    case 'rewindVideo10Sec':
      title = 'Rewinding video 10 seconds';
      break;
    case 'forwardVideo10Sec':
      title = 'Fast forwarding video 10 seconds';
      break;
    default:
      title = 'Unknown';
  }
  return [title, message];
}

export async function getChromeIdentity(): Promise<string> {
  const getAuthTokenPromise: Promise<string> = new Promise((resolve, reject) => {
    console.log('Getting Chrome identity');

    if (!chrome.identity.getAuthToken) {
      reject(new Error('Chrome identity not available (not supported in Edge)'));
    }
    chrome.identity.getAuthToken({ interactive: true }, (token: string) => {
      if (chrome.runtime.lastError) {
        reject(chrome.runtime.lastError);
      } else {
        resolve(token);
      }
    });
  });
  return getAuthTokenPromise;
}
