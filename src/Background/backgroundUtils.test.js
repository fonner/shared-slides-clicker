import { browser } from 'webextension-polyfill-ts';

import { getEmailDomain, getIsEnabled } from './backgroundUtils';

// const testId = '14pIFAJVUX83RF0-F0Oh7AbL1rDLBdY2vHdOHAfBBi1M';
// const testUrl = `https://docs.google.com/presentation/d/${testId}/edit`;
const testUser = { uid: 'testId', email: 'testuser@gmail.com' };

describe('getEmailDomain', () => {
  test('valid email domain will have dot replaced with underscore', () => {
    const domain = getEmailDomain(testUser);
    expect(domain).toBe('gmail_com');
  });
  test('multi-dot domain will have all replaced with underscores', () => {
    const multiDotUser = { uid: 'testId', email: 'test@a.b.com' };
    const domain = getEmailDomain(multiDotUser);
    expect(domain).toBe('a_b_com');
  });
  test('invalid user throws error', () => {
    expect(() => {
      getEmailDomain(null);
    }).toThrow();
  });
  test('invalid email throws error', () => {
    const missingEmailUser = { uid: 'badUser', email: null };
    const domain = getEmailDomain(missingEmailUser);
    console.log('invalid email throws error', domain);
    expect(domain).toBe('');
  });
});

describe('getIsEnabled', () => {
  test('first call', async () => {
    const isEnabled = await getIsEnabled();
    expect(isEnabled).toBe(true);
  });
  test('Setting disabled to true', async () => {
    browser.storage.local.set({ isDisabled: true });
    const isEnabled = await getIsEnabled();
    expect(isEnabled).toBe(false);
  });
  test('Setting disabled to false', async () => {
    browser.storage.local.set({ isDisabled: false });
    const isEnabled = await getIsEnabled();
    expect(isEnabled).toBe(true);
  });
});
