import { browser } from 'webextension-polyfill-ts';
import chrome from 'sinon-chrome/extensions';
import { setTestUser, resetTestUser } from 'firebase/auth';
import { isReadyCheck, sendBrowserNotification } from './background';

beforeAll(() => {
  global.chrome = chrome;
});

afterEach(() => {
  resetTestUser();
});

describe('isReadyCheck', () => {
  test('Initial isReadyCheck should return defaults', async () => {
    setTestUser({ uid: 'testUser' });
    const response = await isReadyCheck();
    expect(response.success).toBe(true);
    expect(response.response.user.uid).toBe('testUser');
    expect(response.response.isEnabled).toBe(true);
    expect(response.response.inAutomaticMode).toBe(true);
  });

  test('isReadyCheck when not logged in yet', async () => {
    setTestUser(null);
    const response = await isReadyCheck();
    expect(response.success).toBe(true);
    expect(response.response.user).toBeNull();
    expect(response.response.isEnabled).toBe(true);
    expect(response.response.inAutomaticMode).toBe(true);
  });
});

// TODO: move to MeetControlHelper.test.js
// describe('checkPresentationStatus', () => {
//   test('No active presentation', async () => {
//     const { success, response } = await checkPresentationStatus(
//       'https://docs.google.com/presentation/d/14pIFAJVUX83RF0-F0Oh7AbL1rDLBdY2vHdOHAfBBi1M/edit'
//     );
//     expect(success).toBe(true);
//     expect(response.isPresentationActive).toBeFalsy();
//   });

//   test('Active presentation', async () => {
//     const user = getTestUser();
//     const domain = getEmailDomain(user);
//     set(`${domain}/14pIFAJVUX83RF0-F0Oh7AbL1rDLBdY2vHdOHAfBBi1M`, 1);

//     const { success, response } = await checkPresentationStatus(
//       'https://docs.google.com/presentation/d/14pIFAJVUX83RF0-F0Oh7AbL1rDLBdY2vHdOHAfBBi1M/edit'
//     );
//     expect(success).toBe(true);
//     expect(response.isPresentationActive).toBeTruthy();
//   });
// });

// TODO: move these to presentation content script test
// describe('ListeningForPresentationActions', () => {
//   const testId = '14pIFAJVUX83RF0-F0Oh7AbL1rDLBdY2vHdOHAfBBi1M';
//   test('startAndStopListeningForPresentationActions', async () => {
//     const startReply = await startActivePresentation(testId, 1);
//     expect(startReply.success).toBe(true);
//     expect(set).toHaveBeenCalled();
//     const stopReply = await stopActivePresentation(testId, 1);
//     expect(stopReply.success).toBe(true);
//     expect(remove).toHaveBeenCalled();
//   });
// });

// describe('slideActions', () => {
//   const serverTimestamp = getServerDatetime();
//   const testId = '14pIFAJVUX83RF0-F0Oh7AbL1rDLBdY2vHdOHAfBBi1M';
//   const testUrl = `https://docs.google.com/presentation/d/${testId}/edit#slide=id.ga0c5ccff23_0_31`;
//   const user = getTestUser();
//   const domain = getEmailDomain(user);
//   const location = `${domain}/${testId}/commands`;

//   test('addSlideAction', async () => {
//     const reply = await addSlideAction(testUrl, 'right');
//     expect(reply.success).toBe(true);
//     expect(push).toHaveBeenCalledWith(location, {
//       action: 'right',
//       user: 'Testee Tester',
//       timestamp: serverTimestamp,
//     });
//   });

//   test('removeSlideAction', async () => {
//     await removeSlideAction(testId, domain, 'right');
//     expect(remove).toHaveBeenCalledWith(`${domain}/${testId}/commands/right`);
//   });
// });

describe('sendBrowserNotification', () => {
  test('basic test', async () => {
    const mockCreate = jest.fn(() => 123);
    browser.notifications = {
      create: mockCreate,
    };
    const {
      response: { notificationId },
    } = await sendBrowserNotification('title', 'message');
    expect(notificationId).toBe(123);
    expect(mockCreate).toHaveBeenCalledTimes(1);
  });
});
