import { browser, resetStorage } from 'webextension-polyfill-ts';

import {
  getIsInAutomaticMode,
  toggleMode,
  getManualButtonStatus,
  getPresentationIdFromUrl,
  handlePresentationManualButtonClick,
  sendMessageToTabById,
  sendMessageToTabByUrl,
  sendManualControlMessageToPresentationTab,
} from './manualModeUtils';

const testId = '14pIFAJVUX83RF0-F0Oh7AbL1rDLBdY2vHdOHAfBBi1M';
const testUrl = `https://docs.google.com/presentation/d/${testId}/edit#slide=id.ga0c5ccff23_0_31`;

describe('getPresentationIdFromUrl', () => {
  test('valid presentation url', () => {
    const url = testUrl;
    const id = getPresentationIdFromUrl(url);
    expect(id).toBe(testId);
  });
  test('presentation url with fragment', () => {
    const url = `${testUrl}#slide=id.ga0c5ccff23_0_31`;
    const id = getPresentationIdFromUrl(url);
    expect(id).toBe(testId);
  });
  test('presentation with query param', () => {
    const url = `https://docs.google.com/presentation/d/${testId}?authuser=epopp%40maxcluster.de&usp=drive_fs`;
    const id = getPresentationIdFromUrl(url);
    expect(id).toBe(testId);
  });
  test('invalid presentation url', () => {
    const consoleSpy = jest.spyOn(console, 'error').mockImplementation();
    const url = `https://meet.google.com/bse-khxz-ctf`;
    const id = getPresentationIdFromUrl(url);
    expect(id).toBeNull();
    expect(consoleSpy).toHaveBeenCalled();
  });
});

test('Default state for isAutomaticMode should be true', async () => {
  const automaticMode = await getIsInAutomaticMode();
  expect(automaticMode).toBe(true);
});

test('Initial toggle call should swap isAutomaticMode from default (true) to false', async () => {
  const message = await toggleMode();
  expect(message.success).toBe(true);
  expect(message.response.isInAutomaticMode).toBe(false);
});

test('sharePresentation', async () => {
  resetStorage();
  await handlePresentationManualButtonClick(testUrl, true);
  expect(browser.storage.local.set).toHaveBeenCalled();
  const browserStorage = browser.storage.local.get();
  expect(browserStorage).not.toBe({});
});

test('unsharePresentation', async () => {
  resetStorage();
  await handlePresentationManualButtonClick(testUrl, true);
  let browserStorage = browser.storage.local.get();
  expect(browserStorage).not.toBe({});
  await handlePresentationManualButtonClick(testUrl, false);
  browserStorage = browser.storage.local.get();
  expect(browserStorage).toMatchObject({});
});

test('wasManualButtonClicked', async () => {
  resetStorage();
  await handlePresentationManualButtonClick(testUrl, true);
  const {
    success,
    response: { wasClicked },
  } = await getManualButtonStatus(testUrl);
  expect(success).toBe(true);
  expect(wasClicked).toBe(true);
});

describe('sendMessages', () => {
  test('sendMessageToTabById', async () => {
    await sendMessageToTabById(123, 'message');
    expect(browser.tabs.query).toHaveBeenCalledTimes(0);
    expect(browser.tabs.sendMessage).toHaveBeenCalledTimes(1);
  });
  test('sendMessageToTabByUrl', async () => {
    await sendMessageToTabByUrl(testUrl, 'message');
    expect(browser.tabs.query).toHaveBeenCalledTimes(1);
    expect(browser.tabs.sendMessage).toHaveBeenCalledTimes(1);
  });
  test('sendShareControlMessageToPresentationTab', async () => {
    await sendManualControlMessageToPresentationTab(testUrl, true);
    expect(browser.tabs.query).toHaveBeenCalledTimes(1);
    expect(browser.tabs.sendMessage).toHaveBeenCalledTimes(1);
  });
});
