import { browser } from 'webextension-polyfill-ts';
import './styles.scss';

interface OptionToggleProps {
  label: string;
  enabledMessage: string;
  disabledMessage: string;
  isEnabled: boolean;
  doBackgroundToggle: React.MouseEventHandler & React.KeyboardEventHandler;
  isDisabled?: boolean;
}

export const OptionToggle = ({
  label,
  enabledMessage,
  disabledMessage,
  isEnabled,
  doBackgroundToggle,
  isDisabled = false,
}: OptionToggleProps): JSX.Element => {
  let iconPath = 'assets/icons/';
  if (isEnabled) {
    if (isDisabled) {
      iconPath += 'toggled-on-disabled.png';
    } else {
      iconPath += 'toggled-on.png';
    }
  } else {
    iconPath += 'toggled-off.png';
  }
  return (
    <div
      className={`ssc_optionsBox ${isDisabled ? 'disabled' : ''}`}
      onClick={isDisabled ? undefined : doBackgroundToggle}
      onKeyPress={isDisabled ? undefined : doBackgroundToggle}
    >
      <div className="ssc_optionsToggleContainer">
        <label htmlFor="isEnabled">{label}</label>
        <div className="ssc_toggleButtons">
          <img
            data-name="isEnabled"
            src={browser.runtime.getURL(iconPath)}
            alt={isEnabled ? 'On' : 'Off'}
            title={isEnabled ? 'On' : 'Off'}
          />
        </div>
      </div>
      <div className="ssc_optionsSecondaryText">
        {isEnabled && <span>{enabledMessage}</span>}
        {!isEnabled && <span>{disabledMessage}</span>}
      </div>
    </div>
  );
};
