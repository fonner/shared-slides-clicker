import React from 'react';
import { browser } from 'webextension-polyfill-ts';
import './styles.scss';

export interface TextAndCopyButtonProps {
  textToCopy?: string;
  caption: string;
  wasCopied?: boolean;
  callback?: (wasCopied: boolean) => void;
}

const copyTextToClipboard = (text: string, callback?: (wasCopied: boolean) => void) => {
  return navigator.clipboard
    .writeText(text)
    .then(() => {
      if (callback) {
        callback(true);
      }
      return true;
    })
    .catch((e) => {
      console.warn('Unable to copy text to clipboard', e);
      if (callback) {
        callback(false);
      }
      return false;
    });
};

export const TextAndCopyButton = ({
  textToCopy = '',
  caption,
  callback,
}: TextAndCopyButtonProps): JSX.Element => {
  const inputRef = React.useRef<HTMLInputElement | null>(null);

  const handleClick = async () => {
    const result: boolean = await copyTextToClipboard(textToCopy, callback);
    if (result && inputRef.current) {
      inputRef.current.className = 'copied';
      inputRef.current.value = 'Copied!';
      setTimeout(() => {
        if (inputRef && inputRef.current) {
          inputRef.current.className = 'standard';
          inputRef.current.value = textToCopy || '';
        }
      }, 1000);
    }
  };

  if (!textToCopy) {
    return <></>;
  }
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '90%',
        marginLeft: '16px',
      }}
    >
      <input ref={inputRef} type="text" readOnly value={textToCopy} style={{ width: '200px' }} />
      &nbsp;
      <button type="button" title={caption} className="ssc_copyButton" onClick={handleClick}>
        <img
          src={browser.runtime.getURL('assets/icons/copy-blue.svg')}
          height="24"
          width="24"
          alt="icon"
          title={`Copy ${caption} to clipboard`}
        />
      </button>
    </div>
  );
};
