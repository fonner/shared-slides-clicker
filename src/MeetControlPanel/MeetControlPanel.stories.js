/* eslint-disable import/no-anonymous-default-export */
/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
// import { forceReRender } from '@storybook/react';

import { MeetControlPanel } from './MeetControlPanel.tsx';

export default {
  title: 'Components/Meet Control Panel',
  component: MeetControlPanel,
  argTypes: {},
};

const Template = (args) => <MeetControlPanel {...args} />;
export const ActivePresentation = Template.bind({});
ActivePresentation.args = {
  presentationUrl: 'https://docs.google.com/presentation/d/1234567890abcdefg/edit',
  keyboardShortcutsEnabled: false,
  isPresentationActive: true,
};

export const DeactivePresentation = Template.bind({});
DeactivePresentation.args = {
  presentationUrl: 'https://docs.google.com/presentation/d/1234567890abcdefg/edit',
  keyboardShortcutsEnabled: false,
  isPresentationActive: false,
};
