/* eslint-disable no-unused-vars */
import React, { ReactElement } from 'react';
import { browser } from 'webextension-polyfill-ts';
import './styles.scss';

interface MeetControlPanelProps {
  isPresentationActive: boolean;
  checkPresentationStatus: (event: React.KeyboardEvent | React.MouseEvent) => void;
  backgroundOpenOptionsPage: (section?: string) => void;
  toggleIsExpanded: (event: React.KeyboardEvent | React.MouseEvent) => void;
  setIsUrlSaved: (isUrlSaved: boolean) => void;
  incrementSlide: () => void;
  decrementSlide: () => void;
  playPauseVideo: () => void;
  rewindVideo: () => void;
  forwardVideo: () => void;
}

export const MeetControlPanel = ({
  isPresentationActive,
  checkPresentationStatus,
  backgroundOpenOptionsPage,
  toggleIsExpanded,
  setIsUrlSaved,
  incrementSlide,
  decrementSlide,
  playPauseVideo,
  rewindVideo,
  forwardVideo,
}: MeetControlPanelProps): JSX.Element => {
  const openOptionsPage = (section: string) => {
    backgroundOpenOptionsPage(section);
  };
  const renderWaiting = (): ReactElement | null => {
    if (isPresentationActive) {
      return null;
    }
    return (
      <div className="ssc_presNotActive">
        <div>
          <p>
            Waiting for presentation...
            <button
              type="button"
              className="ssc_button ssc_inline"
              style={{
                padding: '4px 8px',
                margin: '5px 0 10px 5px',
                fontSize: '0.8em',
                borderRadius: '8px',
                verticalAlign: 'middle',
              }}
              onClick={checkPresentationStatus}
              onKeyPress={checkPresentationStatus}
            >
              <img
                src={browser.runtime.getURL('assets/icons/refresh.svg')}
                alt="Refresh"
                title="Refresh"
                style={{ height: '12px', width: '12px', marginRight: '5px' }}
              />
              Refresh
            </button>
          </p>
        </div>
        <div className="ssc_havingTrouble">
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              openOptionsPage('troubleshooting');
            }}
          >
            Having Trouble?
          </a>
        </div>
      </div>
    );
  };

  const renderButtons = (): ReactElement => {
    const conditionalClassName = isPresentationActive
      ? 'ssc_meetActiveButton'
      : 'ssc_meetDisabledButton';
    return (
      <div className="ssc_buttonsContainer">
        <button
          type="button"
          className="ssc_meetControlButton ssc_meetActiveButton"
          onClick={() => setIsUrlSaved(false)}
          onKeyPress={() => setIsUrlSaved(false)}
        >
          <img
            id="ssc_changeSettingsImg"
            src={browser.runtime.getURL('assets/icons/settings.svg')}
            alt="Change Google Slides URL"
            title="Change Google Slides URL"
          />
        </button>
        <button
          type="button"
          className={`ssc_meetControlButton ${conditionalClassName}`}
          onClick={isPresentationActive ? decrementSlide : undefined}
          onKeyPress={isPresentationActive ? decrementSlide : undefined}
        >
          <img
            id="ssc_previousSlideImg"
            src={browser.runtime.getURL('assets/icons/slide-prev.svg')}
            alt="Previous Slide"
            title="Go to Previous Slide"
          />
        </button>
        <button
          type="button"
          className={`ssc_meetControlButton ${conditionalClassName}`}
          onClick={isPresentationActive ? incrementSlide : undefined}
          onKeyPress={isPresentationActive ? incrementSlide : undefined}
        >
          <img
            id="ssc_nextSlideImg"
            src={browser.runtime.getURL('assets/icons/slide-next.svg')}
            alt="Next Slide"
            title="Go to Next Slide"
          />
        </button>
        <button
          type="button"
          className="ssc_meetControlButton ssc_meetActiveButton"
          onClick={toggleIsExpanded}
          onKeyPress={toggleIsExpanded}
        >
          <img
            id="ssc_remoteImg"
            src={browser.runtime.getURL('assets/icons/remote-blue.svg')}
            style={{ height: '30px' }}
            alt="Collapse Controls"
            title="Collapse presentation controls"
          />
        </button>
        <button
          type="button"
          className={`ssc_meetControlButton ${conditionalClassName}`}
          onClick={isPresentationActive ? rewindVideo : undefined}
          onKeyPress={isPresentationActive ? rewindVideo : undefined}
        >
          <img
            id="ssc_rewindImg"
            src={browser.runtime.getURL('assets/icons/rewind.svg')}
            alt="Rewind Video 10 Seconds"
            title="Rewind Video 10 Seconds"
          />
        </button>
        <button
          type="button"
          className={`ssc_meetControlButton ${conditionalClassName}`}
          onClick={isPresentationActive ? playPauseVideo : undefined}
          onKeyPress={isPresentationActive ? playPauseVideo : undefined}
        >
          <img
            id="ssc_playPauseImg"
            src={browser.runtime.getURL('assets/icons/youtube.svg')}
            alt="Play/Pause Video"
            title="Play/Pause Video"
          />
        </button>
        <button
          type="button"
          className={`ssc_meetControlButton ${conditionalClassName}`}
          onClick={isPresentationActive ? forwardVideo : undefined}
          onKeyPress={isPresentationActive ? forwardVideo : undefined}
        >
          <img
            id="ssc_fastForwardImg"
            src={browser.runtime.getURL('assets/icons/fast-forward.svg')}
            alt="Fast Forward"
            title="Fast Forward Video 10 Seconds"
          />
        </button>
      </div>
    );
  };

  return (
    <div id="ssc_meetcontrolpanel" className="ssc_hoverContainer ssc_horizontallyCentered">
      {renderButtons()}
      {renderWaiting()}
    </div>
  );
};
