import { useState } from 'react';
import { browser } from 'webextension-polyfill-ts';
import * as Sentry from '@sentry/browser';
import { User } from 'firebase/auth';
import { backgroundSignIn, backgroundSignOut } from '../utils/messageUtils';
import './styles.scss';
import MESSAGE_TYPE from '../utils/messageConstants';
import { AuthChangedResponseMessage } from '../@types/messages';

interface SignInButtonProps {
  user: User | null;
  setUser: (user: User | null) => void;
  isLoading?: boolean;
  setShowRefreshMessage: (showMessage: boolean) => void;
}

export const SignInButton = ({
  user,
  setUser,
  isLoading,
  setShowRefreshMessage,
}: SignInButtonProps): JSX.Element => {
  const [signInError, setSignInError] = useState(null as string | null);

  const handleError = (error: Error) => {
    Sentry.captureException(error, {
      extra: { errorMessage: 'SignInButton Error from Background' },
    });
    console.error(`Error from background script: ${error}`);
  };

  const handleAuthChangeResponse = (message: AuthChangedResponseMessage) => {
    if (message && message.success) {
      const currUser = message.response;
      setUser(currUser as User);
      setShowRefreshMessage(message.type === MESSAGE_TYPE.GOOGLE_SIGN_IN && !!currUser);
      console.log(`Authentication Change: user ${currUser ? 'is' : 'is not'} logged in`);
      setSignInError(null);
      if (currUser) {
        window.open('https://meet.google.com/', '_blank');
        window.open(
          'https://docs.google.com/presentation/d/14pIFAJVUX83RF0-F0Oh7AbL1rDLBdY2vHdOHAfBBi1M/view',
          '_blank'
        );
      }
    } else {
      const errorMessage = !!message && `${message.response}`;
      Sentry.captureMessage(`Unable to get Authentication change info: ${errorMessage}`);
      console.info('Unable to get Authentication change info', errorMessage);
      setSignInError(errorMessage);
    }
  };

  const googleSignOut = async () => {
    setUser(null);
    return backgroundSignOut(handleAuthChangeResponse, handleError);
  };
  const googleSignIn = async () => {
    return backgroundSignIn(handleAuthChangeResponse, handleError);
  };

  const renderLoading = () => <div>Loading...</div>;

  const renderLogin = () => (
    <button type="button" className="ssc_button" onClick={googleSignIn} style={{ width: '320px' }}>
      <img
        src={browser.runtime.getURL('assets/icons/log-in.svg')}
        height="32"
        width="32"
        alt="Sign In"
        title="Sign In"
      />
      <p>Sign In</p>
    </button>
  );

  const renderLogout = () => (
    <button
      type="button"
      className="ssc_button secondary"
      onClick={googleSignOut}
      style={{ width: '320px' }}
    >
      <img
        src={browser.runtime.getURL('assets/icons/log-out.svg')}
        height="32"
        width="32"
        alt="Sign Out"
        title="Sign Out"
      />
      <p>Sign Out</p>
    </button>
  );

  const renderUser = () => <div className="ssc_user">Logged in as {user?.email}</div>;

  return (
    <div>
      {isLoading && renderLoading()}
      {!isLoading && !user && renderLogin()}
      {!user && signInError && <div className="ssc_error">{signInError}</div>}
      {!isLoading && user && renderLogout()}
      {user && renderUser()}
    </div>
  );
};
