/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable no-continue */
import ReactDOM from 'react-dom';
import { browser } from 'webextension-polyfill-ts';
import * as Sentry from '@sentry/browser';

import MeetControlWrapper from '../MeetControlWrapper';
import {
  backgroundCheckForReadiness,
  backgroundSignOut,
  backgroundUnshareControl,
  backgroundUpdateIcon,
} from '../utils/messageUtils';
import MESSAGE_TYPE from '../utils/messageConstants';
import { ReadyCheckResponseMessage, MeetControlRequestMessage } from '../@types/messages';
import { getCurrentUser, googleSignInWithToken } from '../Background/firebase';

console.log('Shared Slides Clicker: Meet ContentScript loading...');

const CONTROLLER_ID = 'ssc_controller';
/* SSC uses these to detect changes to Google Meet */
const SIDEBAR_QUERY_OLD = '[jsname="Yz8Ubc"]';
const SIDEBAR_QUERY_NEW = '[jsname="ME4pNd"]';
const SIDEBAR_OPEN_CLASSNAME = [
  'mKBhCf qwU8Me RlceJe kjZr4' /* old Meet */,
  'R3Gmyc qwU8Me' /* new Meet */,
];
const SIDEBAR_CLOSED_CLASSNAME = [
  'mKBhCf qwU8Me RlceJe' /* old Meet */,
  'R3Gmyc qwU8Me qdulke' /* new Meet */,
];
const WARNING_CLASSNAME = 'MON6Vd P9KVBf enYI8';
const START_MEETING_ATTRIBUTE = 'data-participant-id';
const LEFT_MEETING_ATTRIBUTE = 'data-timeout';

/* Local variables for tracking key bits of state */
let intervalId: number;
let listenForManualModeMessages: (request: MeetControlRequestMessage) => Promise<void>;
let keyboardShortcutsOverriden = false;
let isInitialized = false;

/* On load, call backgroundCheckForReadiness to check if SSC is ready */
backgroundCheckForReadiness(
  (message: ReadyCheckResponseMessage) => {
    console.debug('Shared Slides Clicker: Ready check response', message);
    if (message && message.success) {
      const { isEnabled, inAutomaticMode, tabId } = message.response;
      console.log(
        `Shared Slides Clicker: Extension is ${isEnabled ? 'enabled' : 'disabled'} on tab ${tabId}`
      );
      initializeSSC(isEnabled, inAutomaticMode);
    } else {
      console.error('Shared Slides Clicker: Unable to get readiness status');
    }
  },
  (error: Error) => {
    Sentry.captureException(error, {
      extra: { errorMessage: 'Error in backgroundCheckForReadiness' },
    });
    console.error(`Shared Slides Clicker: Error`, error);
  }
);

/* This handles all the SSC behavior and is called by handleReadinessCheckResponse */
async function initializeSSC(isEnabled: boolean, inAutomaticMode: boolean) {
  if (isInitialized) {
    console.warn('Shared Slides Clicker: Already initialized');
    Sentry.captureMessage('Attempt to duplicate initialization in meetContentScript');
    return;
  }
  console.log('Shared Slides Clicker: Initializing');

  if (!isEnabled) {
    console.warn(
      'Shared Slides Clicker: Features are disabled. Please click the extension icon and enable features.'
    );
    return;
  }

  if (!inAutomaticMode) {
    console.info(
      'Shared Slides Clicker: In manual mode. Click extension button in Chrome toolbar to control presentations.'
    );
  }

  let meetAuthUser = await getCurrentUser();
  if (!meetAuthUser) {
    console.log('Shared Slides Clicker: No user signed in, trying to sign in with token');
    const token = await browser.storage.local.get('token');
    await googleSignInWithToken(token.token);
    meetAuthUser = await getCurrentUser();
  }
  if (!meetAuthUser) {
    console.error('Shared Slides Clicker: Unable to sign-in');
    await backgroundSignOut(
      (message) => {
        console.log(
          'Shared Slides Clicker: Forced signout because of authentication problem',
          message
        );
        Sentry.captureMessage(
          'Forced signout because of authentication problem in meetContentScript'
        );
        backgroundUpdateIcon(false);
      },
      (error) => {
        console.error('Error signing out', error);
      }
    );
    return;
  }
  console.log('Shared Slides Clicker: User is signed in', meetAuthUser?.email);

  const removeSSCFromMeet = () => {
    const ssc = document.getElementById(CONTROLLER_ID);
    if (ssc) {
      ReactDOM.render(<></>, ssc); // This ensures unmount lifecycles are run and any listeners are removed
      ssc.remove();
    }
  };

  /*
    injectSSCIntoMeet() adds the MeetControlWrapper to Meet.
    It is called when the MutationObserver detects that the user joined a Meet
  */
  const injectSSCIntoMeet = () => {
    if (isAlreadyInjected()) {
      console.warn('Shared Slides Clicker: Already added to Meet, not adding again.');
      return;
    }
    // Add our button to the Meet toolbar
    const toggleButton = document.createElement('div');
    toggleButton.id = CONTROLLER_ID;
    toggleButton.classList.add('__ssc_controller');

    // Create SSC control DOM
    ReactDOM.render(
      <MeetControlWrapper firebaseUser={meetAuthUser} inAutomaticMode={inAutomaticMode} />,
      toggleButton
    );

    // Finally add the new DOM element tree to the body
    document.body.appendChild(toggleButton);
    console.log('Shared Slides Clicker: Added to Meet');
  };

  /*
    Re-render the MeetControlWrapper with disableKeyboardShortcuts to override keyboard
    shortcuts if the sidepanel is open
  */
  const disableKeyboardShortcuts = (isDisabled: boolean) => {
    const toggleButton = document.getElementById(CONTROLLER_ID);
    ReactDOM.render(
      <MeetControlWrapper
        firebaseUser={meetAuthUser}
        inAutomaticMode={inAutomaticMode}
        disableKeyboardShortcuts={isDisabled}
      />,
      toggleButton
    );
    keyboardShortcutsOverriden = isDisabled;
  };

  const mutationObserverCallback = async (mutationsList: MutationRecord[]) => {
    for (const mutation of mutationsList) {
      if (mutation.type !== 'childList') {
        continue;
      }
      for (const addedNode of mutation.addedNodes) {
        // @ts-ignore // see https://github.com/microsoft/TypeScript-DOM-lib-generator/issues/981
        if (WARNING_CLASSNAME === addedNode.className) {
          console.log('Shared Slides Clicker: Removing confusing warning');
          addedNode.parentElement?.remove();
          // @ts-ignore // see https://github.com/microsoft/TypeScript-DOM-lib-generator/issues/981
        } else if (addedNode.hasAttribute && addedNode.hasAttribute(START_MEETING_ATTRIBUTE)) {
          /* Join Meet screen */
          console.log('Shared Slides Clicker: Detected Meet start');
          if (inAutomaticMode) {
            injectSSCIntoMeet();
          }
          // Don't disconnect because we need to listen for the Left Meeting screen to remove the button
          // obs.disconnect();
          break;
          // @ts-ignore // see https://github.com/microsoft/TypeScript-DOM-lib-generator/issues/981
        } else if (addedNode.hasAttribute && addedNode.hasAttribute(LEFT_MEETING_ATTRIBUTE)) {
          /* Left Meet screen */
          console.log('Shared Slides Clicker: Detected Meet end');
          removeSSCFromMeet();
          if (!inAutomaticMode) {
            // TODO: replace with manual cleanup
            await backgroundUnshareControl(window.location.origin + window.location.pathname);
          }
          obs.disconnect();
          break;
        }
      }
    }
  };

  const obs = new MutationObserver(mutationObserverCallback);
  obs.observe(document.body, {
    childList: true,
    subtree: true,
    attributes: false,
  });

  /*
    Check for sidebar to decide whether to override keyboard shortcuts setting
    This cannot be handled by MutationObserver because the panel is not added
    and removed, it is simply moved offscreen when not used
  */
  intervalId = window.setInterval(async () => {
    const sidebarNewDivList = document.querySelectorAll(SIDEBAR_QUERY_NEW);
    const sidebarOldDivList = document.querySelectorAll(SIDEBAR_QUERY_OLD);
    const sidebarDiv =
      (sidebarNewDivList.length > 0 && sidebarNewDivList[0]) ||
      (sidebarOldDivList.length > 0 && sidebarOldDivList[0]);
    if (!sidebarDiv) {
      return;
    }
    if (!keyboardShortcutsOverriden && SIDEBAR_OPEN_CLASSNAME.indexOf(sidebarDiv.className) >= 0) {
      console.log('Shared Slides Clicker: Overriding Keyboard Shortcuts when Side Panel is open');
      disableKeyboardShortcuts(true);
    } else if (
      keyboardShortcutsOverriden &&
      SIDEBAR_CLOSED_CLASSNAME.indexOf(sidebarDiv.className) >= 0
    ) {
      console.log(
        'Shared Slides Clicker: Removing Keyboard Shortcuts Override when Side Panel is closed'
      );
      disableKeyboardShortcuts(false);
    }
  }, 1000);

  /*
    When not in Automatic mode, the popup panel will send a message via the
    backgroundPage to turn off/on SSC.  Listen for those messages.
  */
  if (!inAutomaticMode) {
    listenForManualModeMessages = (request: MeetControlRequestMessage) => {
      const { type, meetUrl, startControl } = request;
      const currentUrl = window.location.origin + window.location.pathname;
      if (
        (type === MESSAGE_TYPE.SHARE_CONTROL || type === MESSAGE_TYPE.UNSHARE_CONTROL) &&
        meetUrl === currentUrl
      ) {
        console.log(
          `Shared Slides Clicker: Manually ${startControl ? 'adding' : 'removing'} overlay`
        );
        if (startControl) {
          injectSSCIntoMeet();
        } else {
          removeSSCFromMeet();
        }
        return Promise.resolve();
      }
      console.log('Shared Slides Clicker: Ignoring message meant for a different Meet');
      return Promise.resolve();
    };
    browser.runtime.onMessage.addListener(listenForManualModeMessages);
  }

  /* Shut it all down if presentation isn't stopped before closing the window */
  window.addEventListener('pagehide', () => {
    console.log('Shared Slides Clicker: Unloading...');
    // TODO: replace this with
    backgroundUnshareControl(window.location.origin + window.location.pathname);
    obs.disconnect();
    if (intervalId) {
      clearInterval(intervalId);
    }
    if (listenForManualModeMessages) {
      browser.runtime.onMessage.removeListener(listenForManualModeMessages);
    }
  });

  isInitialized = true;
}

const isAlreadyInjected = () => {
  const controllerEl = document.getElementById(CONTROLLER_ID);
  return !!controllerEl;
};

export {};
