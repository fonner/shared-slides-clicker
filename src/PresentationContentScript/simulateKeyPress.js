console.log('Shared Slides Clicker: Adding simulateKeyPress to window');
let lastCommandMilli = null;
function simulateKeyPress(cmd) {
  let keyEvent;
  /* Prevent duplicate clicks by checking if they come in super close together */
  if (lastCommandMilli && Date.now() - lastCommandMilli < 50) {
    console.warn('Shared Slides Clicker: Ignoring duplicate command');
    return;
  }
  lastCommandMilli = Date.now();
  // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code
  // http://www.javascriptkeycode.com/
  switch (cmd) {
    case 'left':
      keyEvent = new KeyboardEvent('keydown', {
        key: 'ArrowLeft',
        keyCode: 37 /* this is the key part */,
        bubbles: true,
      });
      break;
    case 'right':
      keyEvent = new KeyboardEvent('keydown', {
        key: 'ArrowRight',
        keyCode: 39 /* this is the key part */,
        bubbles: true,
      });
      break;
    case 'playPauseVideo':
      keyEvent = new KeyboardEvent('keydown', {
        key: 'k',
        keyCode: 75 /* this is the key part */,
        bubbles: true,
      });
      break;
    case 'toggleFullScreenVideo':
      keyEvent = new KeyboardEvent('keydown', {
        key: 'f',
        keyCode: 70 /* this is the key part */,
        bubbles: true,
      });
      break;
    case 'rewindVideo10Sec':
      keyEvent = new KeyboardEvent('keydown', {
        key: 'u',
        keyCode: 85 /* this is the key part */,
        bubbles: true,
      });
      break;
    case 'forwardVideo10Sec':
      keyEvent = new KeyboardEvent('keydown', {
        key: 'o',
        keyCode: 79 /* this is the key part */,
        bubbles: true,
      });
      break;
    default:
      console.log('simulateKeyPress: unknown command', cmd);
  }
  const presIframe = document.querySelector('.punch-present-iframe');
  if (!presIframe) {
    return;
  }
  const presDoc = presIframe.contentDocument;
  presDoc.dispatchEvent(keyEvent);
}

console.log('Shared Slides Clicker: Adding event listener for simulateKeyPress');
window.addEventListener('SlideCommandEvent', (event) => {
  console.log('Shared Slides Clicker: Received SlideCommandEvent:', event?.detail?.command);
  simulateKeyPress(event?.detail?.command);
});
