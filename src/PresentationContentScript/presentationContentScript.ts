import { browser, Runtime } from 'webextension-polyfill-ts';
import { DataSnapshot, Unsubscribe, serverTimestamp } from 'firebase/database';
import * as Sentry from '@sentry/browser';
import MESSAGE_TYPE, { SLIDE_SHARE_STATUS } from '../utils/messageConstants';
import {
  backgroundCheckForReadiness,
  backgroundUpdateIcon,
  backgroundSendBrowserNotification,
  backgroundSignOut,
} from '../utils/messageUtils';
import {
  AllRequestMessages,
  AllResponseMessages,
  // JustSuccessResponseMessage,
  ManualShareActionRequestMessage,
  ReadyCheckResponseMessage,
  SlidesShareStatusResponseMessage,
} from '../@types/messages';
import {
  onChildAddedToDatabase,
  onDatabaseValueChange,
  getCurrentUser,
  googleSignInWithToken,
  setInDatabase,
  deleteFromDatabase,
} from '../Background/firebase';
import { buildPresentationActionNotification, getEmailDomain } from '../Background/backgroundUtils';
import { cleanupPresentationManualModeData } from '../Background/manualModeUtils';

console.log('Shared Slides Clicker: Content script loading...');
let isInitialized = false;
let isActive = false;
let errorOccurred = false;
let wasRemotelyControlled = false;
let intervalId: number;
let doShare = true;
let manualModeListenerEnabled: boolean;
let tabId: number | null;

const handleReadinessCheckResponse = (message: ReadyCheckResponseMessage) => {
  console.debug('Shared Slides Clicker: Ready check response', message);
  if (message && message.success) {
    console.log('Shared Slides Clicker: Readiness check successful');
    const { isEnabled, inAutomaticMode, tabId: tabIdFromBackground } = message.response;
    tabId = tabIdFromBackground;

    console.log(
      `Shared Slides Clicker: Extension is ${isEnabled ? 'enabled' : 'disabled'} on tab ${tabId}`
    );
    initializeSSC(isEnabled, inAutomaticMode);
  } else {
    Sentry.captureMessage('Unable to get readiness status in presentationContentScript');
    console.error('Shared Slides Clicker: Unable to get readiness status');
  }
};
const handleError = (error: Error) => {
  Sentry.captureException(error, {
    extra: { errorMessage: 'PresentationContentScript Error from Background' },
  });
  console.error('Shared Slides Clicker: Error', error);
};

backgroundCheckForReadiness(handleReadinessCheckResponse, handleError);

const signInWithToken = async () => {
  console.log('Shared Slides Clicker: Getting token from local storage');
  const token = await browser.storage.local.get('token');
  await googleSignInWithToken(token.token);
  const slidesAuthUser = await getCurrentUser();
  return slidesAuthUser;
};

const isUserActive = async (): Promise<boolean> => {
  let slidesAuthUser = await getCurrentUser();
  if (!slidesAuthUser) {
    console.log('Shared Slides Clicker: no current active user');
    slidesAuthUser = await signInWithToken();
  }
  if (!slidesAuthUser) {
    console.error('Shared Slides Clicker: Unable to sign-in');
    await backgroundSignOut(
      (message) => {
        console.log(
          'Shared Slides Clicker: Forced signout because of authentication problem',
          message
        );
        Sentry.captureMessage(
          'Forced signout because of authentication problem in presentationContentScript'
        );
        backgroundUpdateIcon(false);
      },
      (error) => {
        console.error('Error signing out', error);
      }
    );
    return false;
  }
  return true;
};

async function initializeSSC(isEnabled: boolean, inAutomaticMode: boolean) {
  if (isInitialized) {
    console.warn('Shared Slides Clicker: Already initialized');
    Sentry.captureMessage('Attempt to duplicate initialization in presentationContentScript');
    return;
  }
  console.log('Shared Slides Clicker: Initializing');
  let unsubscribeChildAddedListener: Unsubscribe | null = null;
  let unsubscribePresentationListener: Unsubscribe | null = null;

  /* Check if features are enabled */
  if (!isEnabled) {
    console.warn(
      'Shared Slides Clicker: Features are disabled. Please click the extension icon and enable features.'
    );
    return;
  }

  /* Get user */
  const userIsActive: boolean = await isUserActive();
  const authUser = await getCurrentUser();

  if (!userIsActive || !authUser) {
    console.warn(
      'Shared Slides Clicker: User is not signed in. Please click the extension icon and sign in.'
    );
    return;
  }
  console.log('Shared Slides Clicker: User is signed in', authUser?.email);
  const domain = getEmailDomain(authUser);

  if (!inAutomaticMode) {
    console.info(
      'Shared Slides Clicker: Automatic mode is off, slides will not be automatically shared.'
    );
    doShare = false;
  }

  const { pathname } = window.location;
  const pathPieces = pathname.split('/');
  if (pathPieces.length < 4) {
    console.warn('Shared Slides Clicker: Unable to find presentationId');
    return;
  }
  const presentationId = pathPieces[3];
  const presentationKey = `${domain}/${presentationId}`;

  const listenForManualModeMessages = async (request: ManualShareActionRequestMessage) => {
    const {
      type: messageType,
      presentationId: messagePresentationId,
      startSharing: messageDoShare,
    } = request;
    console.log('listenForManualModeMessages', messageType, messagePresentationId, messageDoShare);
    if (
      (messageType === MESSAGE_TYPE.SHARE_PRESENTATION ||
        messageType === MESSAGE_TYPE.UNSHARE_PRESENTATION) &&
      presentationId === messagePresentationId
    ) {
      console.log(`Shared Slides Clicker: Manual Mode ${messageDoShare}`);
      doShare = messageDoShare;
      console.log(doShare ? 'Manually enabling sharing' : 'Manually disabling sharing');
      return browser.storage.local.set({ sharedPresentationId: messagePresentationId });
    }
    return Promise.resolve();
  };

  const startPresentationControlSharing = async () => {
    await addPresentationToDatabase();
    watchForSlideActions();
    backgroundUpdateIcon(true);
  };

  const stopPresentationControlSharing = async () => {
    unsubscribeChildAddedListener?.();
    unsubscribeChildAddedListener = null;
    unsubscribePresentationListener?.();
    unsubscribePresentationListener = null;
    await deleteFromDatabase(`${presentationKey}`);
    backgroundUpdateIcon(false);
    if (wasRemotelyControlled) {
      backgroundSendBrowserNotification(
        'Stopping presentation control sharing',
        'Presentation control sharing is only available when the presentation is in slideshow mode.'
      );
    }
  };

  const addPresentationToDatabase = async () => {
    const ready = await isUserActive();
    if (!ready) {
      console.warn('User is not active');
      Sentry.captureMessage('User is not active in presentationContentScript');
      throw new Error('User is not active');
    }
    // Setting the whole presentation object wipes old actions intentionally (they're no longer relevant)
    await setInDatabase(presentationKey, {
      info: {
        initiatedBy: authUser?.uid,
        initiatedOn: serverTimestamp(),
        presenter: authUser?.email,
        started: new Date().toString(),
      },
    });
  };

  // TODO: move to before initializeSSC?
  const watchForSlideActions = async () => {
    if (!tabId) {
      throw Error('No tabId provided to watchForSlideActions');
    }
    const ready = await isUserActive();
    if (!ready) {
      console.warn('User is not active');
      backgroundUpdateIcon(false);
      Sentry.captureMessage('User is not active in presentationContentScript');
      return;
    }

    /* This compensates for a case where another user starts sharing a presentation
         and then stops, causing data in the database to be removed even though this user
         is still sharing the presentation.
       */
    if (!unsubscribePresentationListener) {
      console.log(
        `Shared Slides Clicker: Watching Firebase for spurious removal of presentation ${presentationId} on tab ${tabId}`
      );
      unsubscribePresentationListener = await onDatabaseValueChange(presentationKey, (snapshot) => {
        const dataIsMissing = !snapshot.val();
        if (dataIsMissing) {
          console.log(`Presentation ${presentationId} is missing, recreating`);
          addPresentationToDatabase();
        }
      });
    } else {
      console.warn(
        `Already watching Firebase for spurious removal of presentation ${presentationId} on tab ${tabId}`
      );
      Sentry.captureMessage(
        'Duplicate unsubscribePresentationListener in presentationContentScript'
      );
    }
    if (!unsubscribeChildAddedListener) {
      console.log(
        `Shared Slides Clicker: Watching Firebase for slide commands for presentation ${presentationId} on tab ${tabId}`
      );
      unsubscribeChildAddedListener = await onChildAddedToDatabase(
        presentationKey,
        'commands',
        (childSnapshot: DataSnapshot) => {
          const command = childSnapshot.val();
          const { key } = childSnapshot;
          if (command.action) {
            console.log(
              `Shared Slides Clicker: Received command '${command.action}' via Firebase from ${command.user} for presentation ${presentationId}`
            );
            const [title, message] = buildPresentationActionNotification(
              command.action,
              command.user
            );
            backgroundSendBrowserNotification(title, message);
            wasRemotelyControlled = true;
            simulateKeyPress(command.action);
            if (key) {
              deleteFromDatabase(`${presentationKey}/commands/${key}`);
            }
          }
        }
      );
    } else {
      console.warn(
        `Already watching Firebase for slide commands for presentation ${presentationId} on tab ${tabId}`
      );
      Sentry.captureMessage('Duplicate unsubscribeChildAddedListener in presentationContentScript');
    }
  };

  // Inject script with listener into the Presentation page
  console.log('Shared Slides Clicker: Adding keypress script to Google Slides');
  const newScript = document.createElement('script');
  newScript.src = chrome.runtime.getURL('/js/simulateKeyPress.bundle.js');
  (document.head || document.documentElement).appendChild(newScript);

  console.log(`Shared Slides Clicker: Ready (tabId ${tabId})`);

  /*
  Periodically check presentation status.
  Don't use Mutation Observer which sometimes returns false positives
  */
  intervalId = window.setInterval(async () => {
    if (!doShare) {
      /* doShare is true by default. If not in automatic mode, it will be set to false.
         If not in automatic mode, listen for manual mode messages which can set doShare to true.
         Use manualModeListenerEnabled to prevent creating more than one listener.
      */
      if (!manualModeListenerEnabled) {
        console.log('Shared Slides Clicker: Listening for manual mode messages');
        browser.runtime.onMessage.addListener(listenForManualModeMessages);
        manualModeListenerEnabled = true;
      }
      /* if !doShare and isActive, then sharing must have been disabled manually after it started
         so stop sharing
      */
      if (isActive) {
        await stopPresentationControlSharing();
        isActive = false;
        wasRemotelyControlled = false;
        console.log('Shared Slides Clicker: Manually deactivating presentation');
      }
      return;
    }
    // doShare has been enabled, so check the status of presentation mode and start/stop sharing accordingly
    const fullScreenFrame = document.querySelectorAll('.punch-present-iframe');
    const isFullScreened = fullScreenFrame.length !== 0;
    try {
      if (isFullScreened && !isActive) {
        /* If fullscreened and not yet active, start listening */
        console.log(
          'Shared Slides Clicker: Detected the start of presentation mode; starting control sharing.'
        );
        await startPresentationControlSharing();
        isActive = true;
      } else if (!isFullScreened && isActive) {
        /* If no longer fullscreened but still active, shut it all down */
        console.log(
          'Shared Slides Clicker: Detected the end of presentation mode; stopping control sharing.'
        );
        await stopPresentationControlSharing();
        isActive = false;
        wasRemotelyControlled = false;
      }
      errorOccurred = false;
    } catch (error) {
      console.error('Shared Slides Clicker:', error);
      errorOccurred = true;
      if (`${error}` === 'Error: PERMISSION_DENIED: Permission denied') {
        console.log('Shared Slides Clicker: User is not active');
        backgroundUpdateIcon(false);
        Sentry.captureMessage('User is not active in presentationContentScript');
      }
      clearInterval(intervalId);
    }
    /*
      Do nothing if (fullscreened and active) or (not fullscreened and not active)
      Note: startListeningForPresentationActions in the backgroundScript will recreate the
      database record if someone else deletes it.
    */
  }, 3000);

  /* Add a listener for messages from popup */
  async function handleMessage(
    message: AllRequestMessages,
    sender: Runtime.MessageSender /* , _sendResponse */
  ): Promise<void | AllResponseMessages | null> {
    console.log(`Shared Slides Clicker: Received message from popup`, message, sender);
    if (sender.id !== 'hbfioanokcpbblpgfjggelapbcaeppop') {
      console.log('Shared Slides Clicker: Ignoring message from invalid sender');
      return null;
    }
    switch (message.type) {
      case MESSAGE_TYPE.SLIDES_SHARE_STATUS: {
        let status;
        if (errorOccurred) status = SLIDE_SHARE_STATUS.ERROR;
        else status = isActive ? SLIDE_SHARE_STATUS.SHARED : SLIDE_SHARE_STATUS.NOT_SHARED;

        const reply: SlidesShareStatusResponseMessage = {
          type: MESSAGE_TYPE.SLIDES_SHARE_STATUS,
          success: true,
          response: {
            status,
          },
        };
        return reply;
      }
      default: {
        console.log('Shared Slides Clicker: Ignoring invalid message type');
        return null;
      }
    }
  }
  console.log('Shared Slides Clicker: Adding message listener for popup messages');
  browser.runtime.onMessage.addListener(handleMessage);

  /* Shut it all down if presentation isn't stopped before closing the window */
  window.addEventListener('pagehide', () => {
    console.log('Shared Slides Clicker: Unloading...');
    stopPresentationControlSharing();
    cleanupPresentationManualModeData(window.location.origin + window.location.pathname);
    if (manualModeListenerEnabled) {
      browser.runtime.onMessage.removeListener(listenForManualModeMessages);
    }
    clearInterval(intervalId);
  });

  isInitialized = true;
}

function simulateKeyPress(command: string) {
  console.log(
    `Shared Slides Clicker: Dispatching SlideCommandEvent '${command}' to simulate key press`
  );
  const commandEvent = new CustomEvent('SlideCommandEvent', { detail: { command } });
  window.dispatchEvent(commandEvent);
}

export {};
