# Shared Slides Clicker for Google Meet

Allows multiple users to control a Google Slides presentation from within Google Meet

🔨 Made by [@jedfonner](https://jedfonner.com)

## Deprecation Notice 

Well after almost 3 years, Google finally listened to our feedback and added the capability for presenters to share presentation control natively inside Google Meet. You can now add “co-presenters” when you are sharing a Google Slides presentation. For more information, see their [blog post](https://workspaceupdates.googleblog.com/2023/04/co-present-google-slides-google-meet.html).

Therefore, you no longer need this extension! Please uninstall it if you currently have it.

Check out my other projects at [fonner.dev](https://fonner.dev/).

## Features

#### User Features

See https://fonner.gitlab.io/shared-slides-clicker/

#### Developer Features

- Cross Browser Support (Web-Extensions API)
- Automatic build on code changes
- SASS styling
- ES6 modules support
- React UI Library
- Smart reload

**Note**: building for Firefox isn't supported. The extension doesn't work in Firefox because Firebase Google Auth currently doesn't support Firefox extensions 🤕. The extension has not been tested in Opera.

Built on top of the amazing 🚀 [web-extension-starter](https://github.com/abhijithvijayan/web-extension-starter)

## 🏃‍ Quick Start

Ensure you have

- [Node.js](https://nodejs.org) 14+ installed

Then run the following:

- `npm install` to install dependencies.
- `npm start` to start the development server for chrome extension
- `npm run build` to build chrome extension

## Development

- `npm install` to install dependencies.
- To watch file changes in development

  - `npm start`

- Load extension in Chrome:

  - Go to the browser address bar and type `chrome://extensions`
  - Check the `Developer Mode` button to enable it.
  - Click on the `Load Unpacked Extension…` button.
  - Select the `extension/chrome` directory.

## Production

- `npm run build` builds the extension to the `extension/chrome.zip`.

Note: By default the `manifest.json` is set with version `0.0.0`. The webpack loader will update the version in the build with that of the `package.json` version. In order to release a new version, update version in `package.json` and run script.

If you don't want to use `package.json` version, you can disable the option [here](https://github.com/abhijithvijayan/web-extension-starter/blob/e10158c4a49948dea9fdca06592876d9ca04e028/webpack.config.js#L79).

## More Info

See [web-extension-starter](https://github.com/abhijithvijayan/web-extension-starter/blob/react-typescript/README.md) for more development information.

## License

MIT © [Jed Fonner](https://jedfonner.com)

<a href='https://www.buymeacoffee.com/jedfonner' target='_blank'>
  <img height='36' style='border:0px;height:36px;' src='https://bmc-cdn.nyc3.digitaloceanspaces.com/BMC-button-images/custom_images/orange_img.png' border='0' alt='Buy Me a Coffee' />
</a>