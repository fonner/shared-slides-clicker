// eslint-disable-next-line import/no-extraneous-dependencies
import chromeWebstoreUpload from 'chrome-webstore-upload';
import fs from 'fs';

console.log('Starting chrome-store-upload');

const store = chromeWebstoreUpload({
  extensionId: 'hbfioanokcpbblpgfjggelapbcaeppop',
  clientId: process.env.CHROME_STORE_CLIENTID,
  refreshToken: process.env.CHROME_STORE_REFRESHTOKEN,
});

const token = await store.fetchToken();

const myZipFile = fs.createReadStream('./extension/chrome.zip');

console.log('Uploading extension');
const { uploadState } = await store.uploadExisting(myZipFile, token);
// Response is a Resource Representation
// https://developer.chrome.com/webstore/webstore_api/items#resource
console.log('Upload response:', uploadState);

console.log('Publishing extension');

const target = 'default'; // optional. Can also be 'trustedTesters'
const { status, statusDetail } = await store.publish(target, token);
// Response is documented here:
// https://developer.chrome.com/webstore/webstore_api/items/publish
console.log('Publish response:', status, statusDetail);
