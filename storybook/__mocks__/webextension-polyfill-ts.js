// src/__mocks__/webextension-polyfill-ts
// Update this file to include any mocks for the `webextension-polyfill-ts` package
// This is used to mock these values for Storybook so you can develop your components
// outside the Web Extension environment provided by a compatible browser

export const browser = {
  runtime: {
    sendMessage(message) {
      console.log('Mocked send message', message);
      if (!message.type) {
        return Promise.resolve('Message Received');
      }
      switch (message.type) {
        case 'GOOGLE_SIGN_IN':
          return Promise.resolve({
            type: 'GOOGLE_SIGN_IN',
            success: true,
            response: { uid: 'test', displayName: 'Test User' },
          });
        case 'READY_CHECK':
          return Promise.resolve({
            type: 'READY_CHECK',
            success: true,
            response: { isEnabled: true, user: { uid: 'test', displayName: 'Test User' } },
          });
        case 'CHECK_PRESENTATION':
          return Promise.resolve({
            type: 'CHECK_PRESENTATION',
            success: true,
            response: {
              isPresentationActive: !!Math.round(Math.random()),
            },
          });
        case 'CONNECT_TO_PRESENTATION':
          return Promise.resolve({
            type: 'CONNECT_TO_PRESENTATION',
            success: !!Math.round(Math.random()),
          });
        default:
          return Promise.resolve('Message Received');
      }
    },
    getURL(url) {
      console.log('Mocked getURL', url);
      return url;
    },
  },
};
