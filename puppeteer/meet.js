/* eslint-disable */
const { getDebugBrowser, colorLog, COLORS, logTestResult, findInStringArray } = require('./utils');
const { TEST_MEET_URL, TEST_PRESENTATION_URL } = require('./constants');

const onConsoleMessages =
  (consoleMessages, responseMessageTypes, responseMessageFailures) => async (consoleObj) => {
    const messageText = consoleObj.text();
    if (messageText.startsWith('Shared Slides Clicker')) {
      colorLog([{ text: messageText, color: COLORS.cyan }]);
    } else {
      console.log(messageText);
    }
    consoleMessages.push(messageText);
    if (messageText.startsWith('Shared Slides Clicker')) {
      const args = consoleObj.args();
      if (args) {
        for (const arg of args) {
          try {
            const jsonVal = await arg.jsonValue();
            if (
              jsonVal.type &&
              jsonVal.success &&
              responseMessageTypes.includes(jsonVal.type) &&
              jsonVal.success === false
            ) {
              responseMessageFailures.push(jsonVal);
            }
          } catch (error) {
            console.warn('Error parsing console log', error);
          }
        }
      }
    }
  };

const testMeetSetup = async (page) => {
  const consoleMessages = [];
  const consoleMessageFailures = [];
  page.on(
    'console',
    onConsoleMessages(
      consoleMessages,
      ['CHECK_PRESENTATION', 'CONNECT_TO_PRESENTATION'],
      consoleMessageFailures
    )
  );
  await page.goto(TEST_MEET_URL);
  colorLog([{ text: 'Loaded Meet URL ', color: COLORS.green }]);
  await page.waitForTimeout(3000);
  await page.waitForSelector('button.VfPpkd-LgbsSe-OWXEXe-k8QpJ');
  await page.click('button.VfPpkd-LgbsSe-OWXEXe-k8QpJ');
  await page.waitForSelector('#ssc_toggleButtonController');
  await page.click('#ssc_toggleButtonController');
  await page.waitForSelector('#presUrl');
  await page.type('#presUrl', TEST_PRESENTATION_URL);
  await page.waitForSelector('.ssc_button');
  await page.click('.ssc_button');
  await page.waitForTimeout(1000);
  await page.screenshot({ path: 'puppeteer/meet.png' });
  const isInitialized = consoleMessages.includes('Shared Slides Clicker: Added to Meet');
  const isActive = findInStringArray(
    consoleMessages,
    'Shared Slides Clicker: User is logged in and extension is enabled on tab'
  );
  const noResponseMessageFailures = consoleMessageFailures.length === 0;
  const passed = isInitialized && isActive && noResponseMessageFailures;
  logTestResult('Meet Setup Test', passed);
  if (!passed) {
    logTestResult(
      `isInitialized: ${isInitialized}, isActive: ${isActive}, noResponseMessageFailures: ${noResponseMessageFailures}`
    );
  }
  return passed;
};

/* This is only called by test.js because it depends on the presentation being active */
const testMeetButtons = async (page) => {
  const consoleMessages = [];
  const consoleMessageFailures = [];
  page.on(
    'console',
    onConsoleMessages(
      consoleMessages,
      ['CHECK_PRESENTATION', 'CONNECT_TO_PRESENTATION'],
      consoleMessageFailures
    )
  );
  await page.waitForSelector('#ssc_nextSlideImg');
  await page.click('#ssc_nextSlideImg');
  await page.waitForTimeout(500);
  await page.click('#ssc_nextSlideImg');
  await page.waitForTimeout(500);
  await page.click('#ssc_nextSlideImg');
  await page.waitForTimeout(500);
  await page.click('#ssc_previousSlideImg');
  await page.waitForTimeout(500);
  await page.click('#ssc_previousSlideImg');

  const incrementMessages = consoleMessages.filter(
    (msg) => msg === 'Shared Slides Clicker: Incrementing slide'
  );
  const decrementMessages = consoleMessages.filter(
    (msg) => msg === 'Shared Slides Clicker: Decrementing slide'
  );
  const sentCorrectIncrements = incrementMessages.length === 3;
  const sentCorrectDecrements = decrementMessages.length === 2;
  const passed = sentCorrectIncrements && sentCorrectDecrements;
  logTestResult('Meet Buttons Test', passed);
  if (!passed) {
    logTestResult(
      `sentCorrectIncrements: ${sentCorrectIncrements}, sentCorrectDecrements: ${sentCorrectDecrements}`
    );
  }
  return passed;
};

(async () => {
  if (require.main === module) {
    const browser = await getDebugBrowser();
    const page = await browser.newPage();
    await testMeetSetup(page);
    await page.close();
    await browser.close();
  } else {
    module.exports = { testMeetSetup, testMeetButtons };
  }
})();
