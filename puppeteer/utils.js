/* eslint-disable */
const puppeteer = require('puppeteer-core');
const { spawn } = require('child_process');

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function getDebugBrowser() {
  const puppeteerDir = __dirname;
  const extensionDir = `${puppeteerDir}/../extension/chrome`;

  const chromeproc = spawn('/usr/bin/google-chrome-stable', [
    '--remote-debugging-port=9222',
    '--no-first-run',
    '--no-default-browser-check',
    '--auto-open-devtools-for-tabs',
    `--user-data-dir=${puppeteerDir}/temp_profile`,
    `--load-extension=${extensionDir}`,
  ]);
  const chromeLogMessages = [];
  chromeproc.stderr.on('data', (data) => {
    chromeLogMessages.push(data.toString());
  });
  console.log('Spawned Chrome in debug mode');
  await sleep(1000);
  // console.log('chromeLogMessages', chromeLogMessages);
  const matchingMessage = chromeLogMessages
    .filter((msg) => msg.trim().startsWith('DevTools listening on'))
    .pop();
  const pieces = matchingMessage.split('DevTools listening on ');
  const browserWSEndpoint = pieces.pop();

  const browser = await puppeteer.connect({
    browserWSEndpoint,
    defaultViewport: { width: 1500, height: 900 },
  });
  return browser;
}

// https://simplernerd.com/js-console-colors/
const COLORS = {
  red: '\x1b[1;31m',
  green: '\x1b[1;32m',
  yellow: '\x1b[1;33m',
  blue: '\x1b[1;34m',
  purple: '\x1b[1;35m',
  cyan: '\x1b[1;36m',
};
const RESET_COLOR = '\x1b[0m';
function colorLog(messageObjects, data) {
  let message = '';
  for (const msg of messageObjects) {
    const { color, text } = msg;
    message = message.concat(`${color}${text}${RESET_COLOR}`);
  }
  if (data) {
    console.log(message, data);
  } else {
    console.log(message);
  }
}

function logTestResult(testName, passed) {
  colorLog([
    { text: `***** ${testName} RESULT: `, color: COLORS.yellow },
    passed ? { text: 'PASSED', color: COLORS.green } : { text: 'FAILED', color: COLORS.red },
    { text: ' *****', color: COLORS.yellow },
  ]);
}

function findInStringArray(stringArray, searchString) {
  return stringArray.some((str) => str.includes(searchString));
}

module.exports = { getDebugBrowser, colorLog, COLORS, logTestResult, findInStringArray };
