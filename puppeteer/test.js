/* eslint-disable */
const { getDebugBrowser, logTestResult } = require('./utils');
const meet = require('./meet');
const presentation = require('./presentation');

(async () => {
  const browser = await getDebugBrowser();
  const presPage = await browser.newPage();
  const presentationTestResult = await presentation.testPresentation(presPage);
  const meetPage = await browser.newPage();
  const meetSetupTestResult = await meet.testMeetSetup(meetPage);
  const meetButtonsTestResult = await meet.testMeetButtons(meetPage);
  await meetPage.close();
  await presPage.close();
  await browser.close();
  logTestResult(
    'All Tests',
    presentationTestResult && meetSetupTestResult && meetButtonsTestResult
  );
})();
