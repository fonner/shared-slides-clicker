module.exports.TEST_MEET_URL = 'https://meet.google.com/kck-xnwy-csh?authuser=0';
module.exports.TEST_PRESENTATION_ID = '14pIFAJVUX83RF0-F0Oh7AbL1rDLBdY2vHdOHAfBBi1M';
module.exports.TEST_PRESENTATION_URL = `https://docs.google.com/presentation/d/${module.exports.TEST_PRESENTATION_ID}/edit`;
