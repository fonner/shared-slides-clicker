/* eslint-disable */
const { getDebugBrowser, colorLog, COLORS, logTestResult, findInStringArray } = require('./utils');
const { TEST_PRESENTATION_ID, TEST_PRESENTATION_URL } = require('./constants');

const testPresentation = async (page) => {
  const consoleMessages = [];
  page.on('console', (consoleObj) => {
    const messageText = consoleObj.text();
    if (messageText.startsWith('Shared Slides Clicker')) {
      colorLog([{ text: messageText, color: COLORS.cyan }]);
    } else {
      console.log(messageText);
    }
    consoleMessages.push(messageText);
  });
  await page.goto(TEST_PRESENTATION_URL);
  colorLog([{ text: 'Loaded Presentation URL ', color: COLORS.green }]);
  await page.click('.punch-start-presentation-container');
  await page.waitForTimeout(500);
  /* Drop out of full-screen mode by entering CMD/CTRL+SHIFT+F */
  await page.keyboard.down('Control');
  await page.keyboard.down('Shift');
  await page.keyboard.press('KeyF');
  await page.keyboard.up('Shift');
  await page.keyboard.up('Control');

  await page.waitForTimeout(3000);
  await page.waitForSelector('.punch-present-iframe');

  const isInitialized = findInStringArray(consoleMessages, 'Shared Slides Clicker: Initializing');
  const isActive = findInStringArray(
    consoleMessages,
    'Shared Slides Clicker: Detected the start of presentation mode; starting control sharing.'
  );
  const isWatchingFirebase =
    findInStringArray(
      consoleMessages,
      `Shared Slides Clicker: Watching Firebase for spurious removal of presentation ${TEST_PRESENTATION_ID} on tab`
    ) &&
    findInStringArray(
      consoleMessages,
      `Shared Slides Clicker: Watching Firebase for slide commands for presentation ${TEST_PRESENTATION_ID} on tab`
    );

  const passed = isInitialized && isActive && isWatchingFirebase;

  logTestResult('Presentation Startup Test', passed);
  if (!passed) {
    logTestResult(
      `isInitialized: ${isInitialized}, isActive: ${isActive}, isWatchingFirebase: ${isWatchingFirebase}`
    );
  }
  return passed;
};

(async () => {
  if (require.main === module) {
    const browser = await getDebugBrowser();
    const page = await browser.newPage();
    await testPresentation(page);
    await page.close();
    await browser.close();
  } else {
    module.exports.testPresentation = testPresentation;
  }
})();
