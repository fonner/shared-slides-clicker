module.exports = {
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: ['@storybook/preset-scss', '@storybook/addon-links', '@storybook/addon-essentials'],
  webpackFinal: (config) => {
    config.resolve.alias['webextension-polyfill-ts'] = require.resolve(
      '../storybook/__mocks__/webextension-polyfill-ts.js'
    );

    return config;
  },
};
