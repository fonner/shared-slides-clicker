import '!style-loader!css-loader!sass-loader!../src/styles/_fonts.scss';
import '!style-loader!css-loader!sass-loader!../src/styles/_ssc_common.scss';
import '!style-loader!css-loader!sass-loader!../src/styles/_variables.scss';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};
